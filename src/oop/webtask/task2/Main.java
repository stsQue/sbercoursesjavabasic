package oop.webtask.task2;

public class Main {
    public static void main(String[] args) {
        Stock stock = new Stock("SBER", "ПАО Сбербанк", 281, 282);
        System.out.println(stock.symbol + " " + stock.name + " " + stock.previousClosingPrice + " " + stock.currentPrice);
        System.out.println(stock.getChangePercent());
    }
}
