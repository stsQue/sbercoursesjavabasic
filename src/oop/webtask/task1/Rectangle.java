package oop.webtask.task1;

public class Rectangle {
    double width;
    double height;

    // конструкторы
    Rectangle() {
    }
    Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    // геттеры и сеттеры
    double getArea () {
        return width * height;
    }
    double getPerimetr() {
        return 2 * (width + height);
    }
}
