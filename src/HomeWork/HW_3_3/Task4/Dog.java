package HomeWork.HW_3_3.Task4;

public class Dog {
    private String [] dogNickName;
    private Participant participant;
    private int [][] grade;
    private double [] result;

    public Dog(){}

    public String[] getDogNickName() {
        return dogNickName;
    }

    public void setDogNickName(String[] dogNickName) {
        this.dogNickName = dogNickName;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public int[][] getGrade() {
        return grade;
    }

    public void setGrade(int[][] grade) {
        this.grade = grade;
    }

    public double[] getResult() {
        return result;
    }

    public void setResult(double[] result) {
        this.result = result;
    }
}

