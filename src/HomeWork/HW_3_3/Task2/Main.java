package HomeWork.HW_3_3.Task2;

public class Main {
    public static void main(String[] args) {
        Thing thing = new Thing();
        Thing thing1 = new Stool();
        Thing thing2 = new Table();

        System.out.println(Check.checkedThing(thing));
        System.out.println(Check.checkedThing(thing1));
        System.out.println(Check.checkedThing(thing2));
    }
}
