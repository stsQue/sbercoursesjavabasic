package HomeWork.HW_3_3.Task1;

public class Main {
    public static void main(String[] args) {
        Bat bat = new Bat();
        System.out.println("Летучая мышь");
        bat.Eat();
        bat.Sleep();
        bat.Move();
        bat.WayOfBirth();
        System.out.println();

        Fish fish = new Fish();
        System.out.println("Золотая рыбка");
        fish.Eat();
        fish.Sleep();
        fish.Move();
        fish.WayOfBirth();
        System.out.println();

        Dolphin dolphin = new Dolphin();
        System.out.println("Дельфин");
        dolphin.Eat();
        dolphin.Sleep();
        dolphin.WayOfBirth();
        dolphin.Move();
        System.out.println();

        Eagle eagle = new Eagle();
        System.out.println("Орел");
        eagle.Eat();
        eagle.Sleep();
        eagle.WayOfBirth();
        eagle.Move();

    }
}
