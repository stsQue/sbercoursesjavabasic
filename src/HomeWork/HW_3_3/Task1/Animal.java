package HomeWork.HW_3_3.Task1;

public abstract class Animal {
    /**
     * Есть
     */
    final void Eat () {
        System.out.println("Ест");
    }

    /**
     * Спать
     */
    final void Sleep () {
        System.out.println("Спит");
    }

    /**
     * Рождение
     */
    abstract public void WayOfBirth ();

    /**
     * Перемещение
     */
    abstract public void Move ();
}
