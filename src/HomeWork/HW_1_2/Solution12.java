package HomeWork.HW_1_2;/*
(2 балла) У Марата был взломан пароль. Он решил написать программу,
которая проверяет его пароль на сложность. В интернете он узнал, что пароль
должен отвечать следующим требованиям:
● пароль должен состоять из хотя бы 8 символов;
● в пароле должны быть:
○ заглавные буквы
○ строчные символы
○ числа
○ специальные знаки(_*-)
Если пароль прошел проверку, то программа должна вывести в консоль строку пароль
надежный, иначе строку: пароль не прошел проверку
 */


import java.util.*;

public class Solution12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String password = input.nextLine();
        int upperChar = 0, lowerChar = 0, number = 0, symbols = 0;

        if (password.length() >= 8) {
            for (int i = 0; i < password.length()-1; i++) {
                char checkChar = password.charAt(i);
                if (Character.isUpperCase(checkChar)) {
                    upperChar++;
                }
                if (Character.isLowerCase(checkChar)) {
                    lowerChar++;
                }
                if (Character.isDigit(checkChar)) {
                    number++;
                }
                if (checkChar == '_' || checkChar =='-' || checkChar =='*') {
                    symbols++;
                }
            }
            if (upperChar >= 1 && lowerChar >= 1 && number >= 1 && symbols >= 1) {
                System.out.println("пароль надежный");
            } else {
                System.out.println("пароль не прошел проверку");
            }
        }

    }


}
