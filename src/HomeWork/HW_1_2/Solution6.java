package HomeWork.HW_1_2;/*
(1 балл) На следующий день на работе Петю и его коллег попросили заполнить
анкету. Один из вопросов был про уровень владения английского. Петя и его
коллеги примерно представляют, сколько они знают иностранных слов. Также у
них есть табличка перевода количества слов в уровень владения английском
языком. Было бы здорово автоматизировать этот перевод!

На вход подается положительное целое число count - количество выученных
иностранных слов. Нужно вывести какому уровню соответствует это количество.
 */
import java.util.*;

public class Solution6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = input.nextInt();
        if (count < 500) {
            System.out.println("beginner");
        } else if (500 <= count && count < 1500) {
            System.out.println("pre-intermediate");
        } else if (1500 <= count && count < 2500) {
            System.out.println("intemediate");
        } else if (2500 <= count && count < 3500) {
            System.out.println("upper-intermediate");
        } else if (count >= 3500) {
            System.out.println("fluent");
        }

    }
}
