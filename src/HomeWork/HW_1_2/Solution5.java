package HomeWork.HW_1_2;/*
(1 балл) Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
нужно проверить, имеет ли предложенное квадратное уравнение решение или
нет.

На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.

Ограничения:
-100 < a, b, c < 100

 */

import java.util.*;

public class Solution5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt(), b = input.nextInt(),
                                 c = input.nextInt();
        double d = Math.pow(b, 2) - 4.0 * a * c;

        if (d >= 0) {
            System.out.println("Решение есть");
        } else {
            System.out.println("Решения нет");
        }

    }
}
