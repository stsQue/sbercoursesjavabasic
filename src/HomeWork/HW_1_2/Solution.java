package HomeWork.HW_1_2;/*
(2 балла) Старый телефон Андрея сломался, поэтому он решил приобрести
новый. Продавец телефонов предлагает разные варианты, но Андрея
интересуют только модели серии samsung или iphone. Также Андрей решил
рассматривать телефоны только от 50000 до 120000 рублей. Чтобы не тратить
время на разговоры, Андрей хочет написать программу, которая поможет ему
сделать выбор.

На вход подается строка – модель телефона и число – стоимость телефона.
Нужно вывести "Можно купить", если модель содержит слово samsung или
iphone и стоимость от 50000 до 120000 рублей включительно. Иначе вывести
"Не подходит".

Гарантируется, что в модели телефона не указано одновременно несколько
серий.
 */
import java.util.*;
import java.util.regex.*;

public class Solution {
    public static void main(String[] args) {
        // получить название модели и цену
        Scanner input = new Scanner(System.in);

        String deviceName = input.nextLine();
        int cost = input.nextInt();

        // обработать полученную информацию
        boolean checkName, checkCost;
        Pattern isCheckIphone = Pattern.compile("iphone");
        Pattern isCheckSamsung = Pattern.compile("samsung");
        checkName = (isCheckIphone.matcher(deviceName.toLowerCase()).find() ||
                      isCheckSamsung.matcher(deviceName.toLowerCase()).find());
        checkCost = (50000 <= cost && cost <= 120000);

        // выдать результат
        if (checkName && checkCost) {
            System.out.println("Можно купить");
        } else {
            System.out.println("Не подходит");
        }


    }
}
