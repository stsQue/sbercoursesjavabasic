package HomeWork.HW_1_2;/*
(2 балла) У Марата был взломан пароль. Он решил написать программу,
которая проверяет его пароль на сложность. В интернете он узнал, что пароль
должен отвечать следующим требованиям:
● пароль должен состоять из хотя бы 8 символов;
Java 12 Базовый модуль Неделя 2
ДЗ 1 Часть 2
● в пароле должны быть:
○ заглавные буквы
○ строчные символы
○ числа
○ специальные знаки(_*-)
Если пароль прошел проверку, то программа должна вывести в консоль строку пароль надежный, иначе строку: пароль не прошел проверку
 */

import java.util.*;
import java.util.regex.*;

public class Solution12_1 {
    public static void main(String[] args) {
        boolean hasDigit, hasUpper, hasLower, hasSpecial;

        Pattern isDigit = Pattern.compile("\\d");
        Pattern isUpper = Pattern.compile("[A-Z]");
        Pattern isLower = Pattern.compile("[a-z]");
        Pattern isSpecial = Pattern.compile("[*-_]");

        Scanner inputPassword = new Scanner(System.in);

        String password = inputPassword.nextLine();

        hasDigit = isDigit.matcher(password).find();
        hasUpper = isUpper.matcher(password).find();
        hasLower = isLower.matcher(password).find();
        hasSpecial = isSpecial.matcher(password).find();

        if (password.length()>=8 && hasDigit && hasUpper && hasLower && hasSpecial) {
            System.out.println("пароль надежный");
        } else {
            System.out.println("пароль не прошел проверку");
        }


    }
}
