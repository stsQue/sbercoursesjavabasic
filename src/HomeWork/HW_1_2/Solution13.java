package HomeWork.HW_1_2;/*
(2 балла) У нас есть почтовая посылка (String mailPackage). Каждая почтовая
посылка проходит через руки проверяющего. Работа проверяющего
заключается в следующем:
● во-первых, посмотреть не пустая ли посылка;
● во-вторых, проверить нет ли в ней камней или запрещенной продукции.

Наличие камней или запрещенной продукции указывается в самой посылке в конце
или в начале. Если в посылке есть камни, то будет написано слово "камни!", если
запрещенная продукция, то будет фраза "запрещенная продукция".
После осмотра посылки проверяющий должен сказать:
● "камни в посылке" – если в посылке есть камни;
● "в посылке запрещенная продукция" – если в посылке есть что-то запрещенное;
● "в посылке камни и запрещенная продукция" – если в посылке находятся камни
и запрещенная продукция;
● "все ок" – если с посылкой все хорошо.
Если посылка пустая, то с посылкой все хорошо.
Напишите программу, которая будет заменять проверяющего.
 */
import java.util.*;
import java.util.regex.*;

public class Solution13 {
    public static void main(String[] args) {
        boolean emptyPackage, stonesInPackage, bannedPackage;
        String message = "все ок";

        // получить данные
        Scanner input = new Scanner (System.in);
        String packageDescription = input.nextLine();

        // проверить данные на пустоту
        Pattern isEmpty = Pattern.compile("[а-яА-Я]");
        emptyPackage = isEmpty.matcher(packageDescription).find();

        if (emptyPackage) {
            // проверить содержание посылки

            Pattern isStones = Pattern.compile("камни");
            stonesInPackage = isStones.matcher(packageDescription).find();

            Pattern isBanned = Pattern.compile("запрещенная продукция");
            bannedPackage = isBanned.matcher(packageDescription).find();

            if (stonesInPackage && bannedPackage) {
                message = "в посылке камни и запрещенная продукция";
            } else if (stonesInPackage){
                message = "камни в посылке";
            } else if (bannedPackage) {
                message = "в посылке запрещенная продукция";
            }
        }
        System.out.println(message);




    }
}
