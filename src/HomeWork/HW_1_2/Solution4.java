package HomeWork.HW_1_2;/*
(1 балл) После вкусного обеда Петя принимается за подсчет дней до выходных.
Календаря под рукой не оказалось, а если спросить у коллеги Феди, то тот
называет только порядковый номер дня недели, что не очень удобно. Поэтому
Петя решил написать программу, которая по порядковому номеру дня недели
выводит сколько осталось дней до субботы. А если же сегодня шестой
(суббота) или седьмой (воскресенье) день, то программа выводит "Ура,
выходные!"
 */


import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int dayNumber = input.nextInt();

        switch (dayNumber) {
            case 1:
            case 5:
            case 4:
            case 3:
            case 2:
                System.out.println(6 - dayNumber);
                break;
            case 6:
            case 7:
                System.out.println("Ура, выходные!");
                break;
        }
    }
}
