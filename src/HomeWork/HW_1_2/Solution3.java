package HomeWork.HW_1_2;/*
(1 балл) Петя снова пошел на работу. С сегодняшнего дня он решил ходить на
обед строго после полудня. Периодически он посматривает на часы (x - час,
который он увидел). Помогите Пете решить, пора ли ему на обед или нет. Если
время больше полудня, то вывести "Пора". Иначе - “Рано”.
0<=n<=23
 */
import java.util.*;

public class Solution3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int time = input.nextInt();
        String message;

        if (time > 0 && time <= 12) {
            message = "Рано";
        } else {
            message = "Пора";
        }
        System.out.println(message);
    }
}
