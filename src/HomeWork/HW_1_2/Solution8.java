package HomeWork.HW_1_2;/*
(1 балл) Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.

Ограничения:
В строке гарантированно есть хотя бы один пробел
 */

import java.util.*;

public class Solution8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String text = input.nextLine();
        int spaceIndex = text.lastIndexOf(" ");
        System.out.println(text.substring(0,spaceIndex) + "\n" + text.substring(spaceIndex + 1, text.length()));

    }
}
