package HomeWork.HW_1_2;/*
(1 балл) За каждый год работы Петя получает на ревью оценку. На вход
подаются оценки Пети за последние три года (три целых положительных числа).
Если последовательность оценок строго монотонно убывает, то вывести "Петя,
пора трудиться"
В остальных случаях вывести "Петя молодец!"
*/
import java.util.Scanner;
public class Solution1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int rating1 = input.nextInt(), rating2 = input.nextInt(), rating3 = input.nextInt();

        if (rating1 > rating2 && rating2 > rating3) {
            System.out.println("Петя, пора трудиться");
        } else {
            System.out.println("Петя молодец!");
        }

    }
}