package HomeWork.HW_1_2;/*
(1 балл) Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().

На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.

Ограничения:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100

 */
import java.util.*;

public class Solution7 {
    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);

       String text = input.nextLine();
       int spaceIndex = text.indexOf(" ");
       System.out.println(text.substring(0,spaceIndex) + "\n" + text.substring(spaceIndex + 1, text.length()));

    }
}
