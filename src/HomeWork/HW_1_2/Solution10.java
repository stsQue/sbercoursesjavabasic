package HomeWork.HW_1_2;/*
(1 балл) "А логарифмическое?" - не унималась дочь.

Напишите программу, которая проверяет, что log(e^n) == n для любого
вещественного n.

Ограничения:
-500 < n < 500
 */
import java.util.*;

public class Solution10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double x = input.nextDouble();
        System.out.println(Math.log(Math.exp(x)) == x);
    }
}
