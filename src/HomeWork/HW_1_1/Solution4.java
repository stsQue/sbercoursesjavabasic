package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution4 {
    public static void main(String[] args) {
        final int SECOND_PER_MINUTE = 60, MINUTE_PER_HOUR = 60, HOUR_PER_DAY = 24;

        Scanner inputSecond = new Scanner(System.in);

        int secondDay = inputSecond.nextInt();
        long minute = secondDay / SECOND_PER_MINUTE % SECOND_PER_MINUTE;
        long hour = secondDay / SECOND_PER_MINUTE / MINUTE_PER_HOUR;

        System.out.printf((int)hour + " " + (int)minute);
    }
}
