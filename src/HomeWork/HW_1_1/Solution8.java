package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int budget = input.nextInt();
        double oneDayBudget = budget / 30.0;

        System.out.print(oneDayBudget);
    }
}