package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int number = input.nextInt(),
                tens = number / 10, one = number % 10;
        System.out.print(one * 10 + tens);

    }
}