package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int budget = input.nextInt();
        int budgetForOneGuest = input.nextInt();

        int numberOfGuests = budget / budgetForOneGuest;

        System.out.print(numberOfGuests);
    }
}
