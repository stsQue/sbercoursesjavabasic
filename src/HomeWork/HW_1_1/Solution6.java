package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        final double KM_IN_MILES = 1.60934;

        double inputKm = input.nextDouble(), convertKmToMiles = inputKm / KM_IN_MILES;
        System.out.print(convertKmToMiles);

    }
}