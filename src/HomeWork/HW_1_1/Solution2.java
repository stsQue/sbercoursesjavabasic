package HomeWork.HW_1_1;

import java.util.*;

public class Solution2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int a = input.nextInt();
        int b = input.nextInt();

        double averageSquare = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);
        System.out.println(averageSquare);

    }
}