package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        final double CENTIMETERS_IN_INCH = 2.54;

        double inputInch = input.nextDouble(), convertToCentimeters = inputInch * CENTIMETERS_IN_INCH;

        System.out.print(convertToCentimeters);

    }
}