package HomeWork.HW_1_1;

import java.util.Scanner;

public class Solution1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int r = input.nextInt();

        double volumeSphere = 4 / 3.0 * Math.PI * Math.pow(r, 3);
        System.out.println(volumeSphere);
    }
}