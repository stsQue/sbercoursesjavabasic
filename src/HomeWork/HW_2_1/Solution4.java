package HomeWork.HW_2_1;

import java.util.*;
import java.util.stream.Collectors;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
*/
public class Solution4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = input.nextInt();
        }
         checkArray(arr);

        for (int i = 0; i < arr.length; i = i + checkArr(arr, arr[i])) {
            System.out.println(checkArr(arr, arr[i]) + " " + arr[i]);
        }
    }

    /**
     * Проверка каждого элемента массива, возвращается кол-во совпадений
     * @param arr
     * @param n
     * @return
     */
    public static int checkArr(int[] arr, int n) {
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (n == arr[i]) {
                counter += 1;
            }
        }
        return counter;

    }

    /**
     * Метод через HashSet и коллекции
     * @param arr
     */
    public static void checkArray(int[] arr) {
        Set<Integer> uniqueValue = Arrays.stream(arr).boxed().collect(Collectors.toSet());
        Integer [] temp = uniqueValue.toArray(new Integer[]{});
        for (int i = 0; i < temp.length; i++) {
            int count = 0;
            for (int j = 0; j < arr.length; j++) {
                if(temp[i] == arr[j])  {
                    count++;
                }
            }
            System.out.println(count + " " + temp[i]);
        }
    }
}
