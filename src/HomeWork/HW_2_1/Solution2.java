package HomeWork.HW_2_1;

import java.util.Arrays;
import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.
 */
public class Solution2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int[] array1 = new int[n];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = input.nextInt();
        }

        int m = input.nextInt();
        int[] array2 = new int[m];
        for (int i = 0; i < array2.length ; i++) {
            array2[i] = input.nextInt();
        }

        boolean compare = compareArray(array1, array2);
        System.out.println(compare);

        System.out.println(Arrays.equals(array1, array2));

    }
    public static boolean compareArray(int[] array1, int[] array2) {
        boolean compare = false;
        if (array1.length == array2.length) {
            for (int i = 0; i < array1.length ; i++) {
                if (array1[i] == array2[i]) {
                    compare = true;
                } else {
                    compare = false;
                    break;
                }
            }
        } else {
            compare = false;
        }
        return compare;
    }
}
