package HomeWork.HW_2_1;

import java.util.Arrays;
import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного, возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
 */
public class Solution7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }

        int[] arrSquared;
        arrSquared = arrayElementToQuadrateAndSort(arr);
        for (int e: arrSquared) {
            System.out.print(e + " ");
        }

    }
    public static int[] arrayElementToQuadrateAndSort(int[] arr) {
        int[] temp = new int[arr.length];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = (int) Math.pow(arr[i], 2);
        }
        Arrays.sort(temp);
        return temp;
    }
}
