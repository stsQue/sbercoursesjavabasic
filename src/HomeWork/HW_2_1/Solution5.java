package HomeWork.HW_2_1;
/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.

Необходимо циклически сдвинуть элементы массива на M элементов вправо.
 */

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arrA = new int[n];

        for (int i = 0; i < arrA.length; i++) {
            arrA[i] = input.nextInt();
        }

        int shift = input.nextInt();
        int[] arrB = new int[arrA.length];
        arrB = arrayShiftRight(arrA, shift);
        for (int e: arrB) {
            System.out.print(e + " ");
        }


    }

    /**
     * Метод сдвига массива вправо через arraycopy
     * @param arr
     * @param shift
     * @return
     */
    public static int[] arrayShiftRight (int[] arr, int shift) {
        int[] arrB = new int[arr.length];
        if (shift > 0) {
            // копируем из arrA элементы с 0 индекса, до n - shift, в индекс shift массива arrB
            System.arraycopy(arr, 0, arrB, shift, arrB.length - shift);
            // копируем из arrA элементы с индекса n - shift, в количестве shift, в индекс 0 массива arrB
            System.arraycopy(arr, arr.length - shift, arrB, 0, shift);
        } else {
            arrB = arr;
        }
        return arrB;
    }
}
