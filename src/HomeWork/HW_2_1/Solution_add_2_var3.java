package HomeWork.HW_2_1;

import java.util.Arrays;
import java.util.Scanner;

/*
(3 балла) Решить задачу 7 основного дз за линейное время.
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного, возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
 */
public class Solution_add_2_var3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        //получаем два массива - исходный и тот что в квадрате
        // считаем количество последовательных отрицательных значений
        int[] arr = new int[in.nextInt()];
        int[] arrQua = new int[arr.length];
        int minusIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
            arrQua[i] = (int) Math.pow(arr[i], 2);
            if (arr[i] < 0) minusIndex++;
        }
        //создаем новый массив
        int[] sortedArrayQua = new int[arrQua.length];
        // заполняем его сортированными значениями
        int i = minusIndex - 1, j = minusIndex, k = 0;
        while (i >= 0 && j < sortedArrayQua.length) {
            if (arrQua[i] < arrQua[j]) {
                sortedArrayQua[k++] = arrQua[i--];
            } else {
                sortedArrayQua[k++] = arrQua[j++];
            }
        }
        //сохраняем оставшиеся элементы, которые не вошли из-за превышения длины счетчика
        while (i >= 0) {
            sortedArrayQua[k++] = arrQua[i--];
        }
        while (j < sortedArrayQua.length) {
            sortedArrayQua[k++] = arrQua[j++];
        }
        System.out.println(Arrays.toString(sortedArrayQua));
    }
}

