package HomeWork.HW_2_1;

import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.

Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего.
 */
public class Solution3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = input.nextInt();
        }

        int x = input.nextInt();
        System.out.println(indexNumber(x, arr));

    }

    private static int indexNumber(int x, int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (x < arr[i]) {
                continue;
            } else if (x == arr[i]) {
                count = i + 1;
            } else  if (x > arr[i]){
                count = i + 1;
            } else {
                break;
            }
        }
        return count;
    }
}
