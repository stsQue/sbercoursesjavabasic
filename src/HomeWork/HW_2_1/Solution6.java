package HomeWork.HW_2_1;

import java.util.Scanner;

/*
(1 балл) На вход подается строка S, состоящая только из русских заглавных
букв (без Ё).

Необходимо реализовать метод, который кодирует переданную строку с
помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
нужно пробелом.
 */
public class Solution6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String inputMessage = input.nextLine();
        String[] morseMessage = codeMessage(inputMessage);
        for (String e: morseMessage) {
            System.out.print(e + " ");
        }


    }
    public static String[] codeMessage (String message) {
        // Создаем два массива одинаковой длины для алфавита и морзянки каждой буквы
        char [] alphabet = {'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М',
                'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ',
                'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};
        String[] morseCode = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..",
                ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...",
                "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-",
                "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        // Создаем массив символов из введенной строки
        char[] messageToChar = message.toCharArray();
        // Создаем массив закодированного сообщения длиной входящей строки
        String [] codeMessage = new String[message.length()];

        // Заполняем закодированное сообщение
        // Сравниваем символы введенного сообщения с алфавитом, если сравнение = 0, то по индексу сравнения добавляем
        // соответствующий индекс морзянки  в codeMessage
        for (int i = 0; i < messageToChar.length; i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (Character.compare(messageToChar[i], alphabet[j])==0) {
                    codeMessage[i] = morseCode[j];
                }
            }
        }
        return codeMessage;
    }
}
