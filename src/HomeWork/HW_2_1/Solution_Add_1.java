package HomeWork.HW_2_1;

import java.util.Scanner;

/*
(2 балла) Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.

Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)
 */
public class Solution_Add_1 {

    private static final String[] LOWER = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    private static final String[] UPPER = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
            "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    private static final String[] DIGITS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private static final String[] SYMBOLS = {"*", "_", "-"};


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите длину желаемого пароля: ");
        int n = in.nextInt();
        while (n < 8) {
            System.out.printf("Пароль с %s количеством символов небезопасен." + "\n"
                    + "Повторите ввод: ", n);
            n = in.nextInt();
        }
        String[] password = generatePassword(n);
        for (String e: password) {
            System.out.print(e);
        }
    }

    /**
     * Метод генерирует пароль
     *
     * @param n
     * @return
     */
    public static String[] generatePassword(int n) {
        String[] password = new String[n];
        //переменные для учета требуемых символов
        boolean isLower = false;
        boolean isUpper = false;
        boolean isDigits = false;
        boolean isSymbols = false;
        int allBoolean = 0;
        //массив заполняется до тех пор, пока не будет хотя бы по одной заглавной буквы,
        //строчной буквы, символа и цифры
        while (allBoolean == 0) {
            for (int i = 0; i < password.length; i++) {
                int random = (int) (Math.random() * 4);
                switch (random) {
                    case 0 -> {
                        password[i] = generateOnePosition(UPPER);
                        isUpper = true;
                    }
                    case 1 -> {
                        password[i] = generateOnePosition(LOWER);
                        isLower = true;
                    }
                    case 2 -> {
                        password[i] = generateOnePosition(DIGITS);
                        isDigits = true;
                    }
                    case 3 -> {
                        password[i] = generateOnePosition(SYMBOLS);
                        isSymbols = true;
                    }
                }
            }
            if (isLower && isUpper && isDigits && isSymbols) allBoolean +=1;
        }
        return password;
    }

    /**
     * Метод возвращает случайный символ из входящего массива
     *
     * @param arr
     * @return
     */
    public static String generateOnePosition(String[] arr) {
        String str = arr[(int) (Math.random() * arr.length)];
        return str;
    }

    static class PasswordChecker {
        boolean isLower = false;
        boolean isUpper = false;
        boolean isDigits = false;
        boolean isSymbols = false;

        public boolean isAllConditions() {
            return isLower && isUpper && isDigits && isSymbols;
        }
    }

}
