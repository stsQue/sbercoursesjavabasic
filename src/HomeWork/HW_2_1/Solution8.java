package HomeWork.HW_2_1;

import java.util.Arrays;
import java.util.Scanner;

/*
 (1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
 */
public class Solution8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //int n = input.nextInt();
        int[] arr = new int[input.nextInt()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = input.nextInt();
        }
        int m = input.nextInt();
        int[] arrayTemp = arrayWithM(arr, m);
        System.out.println(nearM(arrayTemp, m));
    }

    public static int[] arrayWithM (int[] arr, int m) {
        int[] arrWithM = new int[arr.length + 1];
        System.arraycopy(arr, 0, arrWithM, 0, arr.length);
        arrWithM[arrWithM.length - 1] = m;
        Arrays.sort(arrWithM);
        return arrWithM;
    }

    public static int nearM (int[] arr, int m) {
        int previous = 0;
        int next = 0;
        int searchValue = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == m) {
                if (i > 0 && i < arr.length - 1) {
                    previous = arr[i - 1];
                    next = arr[i + 1];
                } else if (i == 0) {
                    next = arr[i + 1];
                    previous = Integer.MIN_VALUE;
                } else {
                    next = Integer.MAX_VALUE;
                    previous = arr[i -1];
                }
            }
        }
        int differencePrevious = m - previous > 0 ? m - previous : -1 * (m - previous);
        int differenceNext = m - next > 0 ? m - next : -1 * (m - next);
        if (differencePrevious < differenceNext) {
            searchValue = previous;
        } else {
            searchValue = next;
        }
        return searchValue;
    }
}
