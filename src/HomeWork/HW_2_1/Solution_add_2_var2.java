package HomeWork.HW_2_1;

import java.util.Scanner;

/*
 (3 балла) Решить задачу 7 основного дз за линейное время.
 (1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного, возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
 */
public class Solution_add_2_var2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        //получить исходный массив
        int[] arr = new int[in.nextInt()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }
        //создать два массива - отрицательных и положительных
        int[] arrMinusValue = onlyPlusMinusInArray(arr);
        int[] arrPlusValue = onlyPlusValueInArray(arr);
        //возвести в квадрат оба массива
        arrMinusValue = arrToQua(arrMinusValue);
        arrPlusValue = arrToQua(arrPlusValue);
        //реверсировать массив отрицательных значений
        arrMinusValue = reverse(arrMinusValue);
        //создать новый массив длиной исходного массива и заполнить новый массив сортированными значениями
        int[] arrInQuaSorted = mergeTwoArrayAndSorted(arrMinusValue, arrPlusValue);
        for (int e: arrInQuaSorted) {
            System.out.print(e + " ");
        }
    }

    /**
     * Массив объединяет два отсортированных массива в третий
     * @param arrMinusValue
     * @param arrPlusValue
     * @return
     */
    private static int[] mergeTwoArrayAndSorted(int[] arrMinusValue, int[] arrPlusValue) {
        int[] mergedArray = new int[arrMinusValue.length + arrPlusValue.length];
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < arrMinusValue.length && j < arrPlusValue.length) {
            if (arrMinusValue[i] < arrPlusValue[j]) {
                mergedArray[k++] = arrMinusValue[i++];
            } else {
                mergedArray[k++] = arrPlusValue[j++];
            }
        }
        //mergedArray[k++] = arr1[i] < arr2[j] ? arr1[i++] : arr2[j++];
        //сохраняем оставшиеся элементы первого массива
        while (i < arrMinusValue.length) {
            mergedArray[k++] = arrMinusValue[i++];
        }
        while (j < arrPlusValue.length) {
            mergedArray[k++] = arrPlusValue[j++];
        }
        return mergedArray;
    }

    /**
     * Реверсирование отрицательного массива
     * @param arr
     * @return
     */
    public static int[] reverse (int[] arr) {
        int[] reverseMassive = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            reverseMassive[i] = arr[arr.length - 1 - i];
        }
        return reverseMassive;
    }

    /**
     * Метод возведения массива в квадрат
     * @param arr
     * @return
     */
    public static int[] arrToQua (int[] arr){
        int[] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = (int) Math.pow(arr[i], 2);
        }
        return arr2;
    }
    /**
     * Возвращает массив отрицательных значений из исходного массива
     * @param arr
     * @return
     */
    private static int[] onlyPlusMinusInArray(int[] arr) {
        int[] onlyMinusValueArray = new int[minusCountInArray(arr)];
        System.arraycopy(arr, 0, onlyMinusValueArray, 0, minusCountInArray(arr));
        return onlyMinusValueArray;
    }

    /**
     * Возвращает только массив положительных значений из исходного массива
     * @param arr
     * @return
     */
    public static int[] onlyPlusValueInArray (int[] arr) {
        int[] onlyPlusValueArray = new int[arr.length - minusCountInArray(arr)];
        System.arraycopy(arr, minusCountInArray(arr), onlyPlusValueArray, 0, onlyPlusValueArray.length);
        return onlyPlusValueArray;
    }

    /**
     * Возвращает количество отрицательных значений в сортированном массиве
     *
     * @param arr
     * @return
     */
    public static int minusCountInArray(int[] arr) {
        int minusCount = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                minusCount++;
            } else {
                break;
            }
        }
        return minusCount;
    }
}


