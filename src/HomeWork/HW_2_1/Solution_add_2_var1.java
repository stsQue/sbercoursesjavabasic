package HomeWork.HW_2_1;

import java.util.Scanner;

/*
 (3 балла) Решить задачу 7 основного дз за линейное время.
 (1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного, возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
 */
public class Solution_add_2_var1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }
        //первый массив в квадрат
        int[] arrInQua = arrToQua(arr);
        // вспомогательная переменная
        int temp = 0;
        // счетчик отрицательных значений
        int i = 0;
        // счетчик перестановки значений возведенных в квадрат
        int j = 0;
        // так как изначальный массив сортирован по возрастанию, то в массиве значений, возведенных в квадрат
        // нам надо отсортировать только те индексы, которые соответствуют положениям отрицательных значений
        // в исходном массиве

        // если значение исходного массива меньше нуля, значит требуется перестановка
        while (arr[i] < 0) {
            //вводим проверку j < arr.length - 1 чтобы не вылететь за пределы массива
            if (j < arr.length - 1) {
                //сортируем отрицательные элементы
                if (arrInQua[j] > arrInQua[j + 1]) {
                    temp = arrInQua[j + 1];
                    arrInQua[j + 1] = arrInQua[j];
                    arrInQua[j] = temp;
                    for (int e: arrInQua) {
                        System.out.print(e + " ");
                    }
                    System.out.println();
                } else {
                    // если перестановка не требуется прибавляем индекс
                    j++;
                }
            } else {
                //увеличиваем индекс исходного массива
                i++;
                //сбрасываем счетчик индексов в полученном массиве
                j = 0;
            }
        }

            for (int e : arrInQua) {
                System.out.print(e + " ");
            }

        }

    /**
     * Метод возведения массива в квадрат
     * @param arr
     * @return
     */
        public static int[] arrToQua (int[] arr){
            int[] arr2 = new int[arr.length];
            for (int i = 0; i < arr.length; i++) {
                arr2[i] = (int) Math.pow(arr[i], 2);
            }
            return arr2;
        }
    }


