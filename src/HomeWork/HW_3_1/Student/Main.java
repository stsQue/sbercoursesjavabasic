package HomeWork.HW_3_1.Student;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Student student = new Student();
        System.out.print("Введите имя студента: ");
        student.setName(in.next());
        System.out.print("Введите фамилию студента: ");
        student.setSurname(in.next());
        System.out.print("Введите оценки:");
        // можно сделать 10 перегружаемых методов setGrades не для массива, а для последовательно введенных через запятую цифр
        // но через массив удобнее
        int[] inputGrades = student.fillGrades();
        // выводим информацию объекта
        System.out.println(student.getName() + " " + student.getSurname() + " " + Arrays.toString(student.getGrades()) + " " + student.averageScore());
        // добавляем оценку
        student.addScore();
        // обновляем информацию
        System.out.println(student.getName() + " " + student.getSurname() + " " + Arrays.toString(student.getGrades()) + " " + student.averageScore());
    }
}
