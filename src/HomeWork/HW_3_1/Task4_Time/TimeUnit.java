package HomeWork.HW_3_1.Task4_Time;

/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).
 */
public class TimeUnit {
    private int second;
    private int minute;
    private int hour;

    //конструктор h m s
    public TimeUnit(int hour, int minute, int second) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else {
            throw new IllegalArgumentException("Введено некорректное значение \"Часы\".");
        }
        if (minute >= 0 && minute <= 59) {
            this.minute = minute;
        } else {
            throw new IllegalArgumentException("Введено некорректное значение \"Минуты\".");
        }
        if (second >= 0 && second <= 59) {
            this.second = second;
        } else {
            throw new IllegalArgumentException("Введено некорректное значение \"Секунды\".");
        }
    } // public TimeUnit(int hour, int minute, int second)

    //конструктор без секунд
    public TimeUnit(int hour, int minute) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else {
            throw new IllegalArgumentException("Введено некорректное значение \"Часы\".");
        }
        if (minute >= 0 && minute <= 59) {
            this.minute = minute;
        } else {
            throw new IllegalArgumentException("Введено некорректное значение \"Минуты\".");
        }
    } //TimeUnit with 0 second

    //Конструктор без минут и секунд
    public TimeUnit(int hour) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else {
            throw new IllegalArgumentException("Введено некорректное значение \"Часы\".");
        }
    } //TimeUnit with 0 minute 0 second

    // метод выводит время в 24 часовом формате
    public String getTime() {
        String hour, minute, second;
        if (this.hour < 10) hour = "0" + Integer.toString(this.hour);
        else hour = Integer.toString(this.hour);
        if (this.minute < 10) minute = "0" + Integer.toString(this.minute);
        else minute = Integer.toString(this.minute);
        if (this.second < 10) second = "0" + Integer.toString(this.second);
        else second = Integer.toString(this.second);
        return hour + ":" + minute + ":" + second;
    } //getTime()

    // метод выводит время в 12 часовом формате
    public String getTime12() {
        String hour, minute, second, ampm;
        if (this.hour % 12 < 10) hour = "0" + Integer.toString(this.hour % 12);
        else hour = Integer.toString(this.hour % 12);
        if (this.minute < 10) minute = "0" + Integer.toString(this.minute);
        else minute = Integer.toString(this.minute);
        if (this.second < 10) second = "0" + Integer.toString(this.second);
        else second = Integer.toString(this.second);
        if (this.hour <= 12) ampm = "AM";
        else ampm = "PM";
        return hour + ":" + minute + ":" + second + " " + ampm;
    } //getTime12()

    // метод прибавляет часы, минут, секунды
    public void addTIme(int hour, int minute, int second) {
        if (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59 && second >= 0 && second <= 59) {
            if (this.second + second > 59) {
                minute++;
                this.second = this.second + second - 60;
            } else {
                this.second += second;
            }
            if (this.minute + minute > 59) {
                hour++;
                this.minute = this.minute + minute - 60;
            } else {
                this.minute += minute;
            }
            if (this.hour + hour <= 23) {
                this.hour = hour;
            } else {
                this.hour = (this.hour + hour) % 23;
            }
        }  else { // если час, минуты, секунды соответствуют размерности
            throw new IllegalArgumentException("Неправильно введены значения");
        }
    } //addTime()
}
