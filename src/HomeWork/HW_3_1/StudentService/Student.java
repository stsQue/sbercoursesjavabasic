package HomeWork.HW_3_1.StudentService;

import java.util.Scanner;

/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
public class Student {
    private String name;
    private String surname;
    private int[] grades;

    //конструктор c аргументами
        Student(String name, String surname, int[] grades){
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }
    //конструктор без аргументов
    Student(){
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public int[] getGrades() {
        return grades;
    }
    public void setGrades(int[] grades) {
        this.grades = grades;
    }
    /**
     * Метод заполнения массива оценок
     *
     * @return
     */
    public int[] fillGrades() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите не более 10 оценок через пробел " +
                "\n" + "(если оценок меньше 10 введите 0 для остановки ввода): ");
        int[] fillGrades = new int[1];
        int n = scanner.nextInt();
        fillGrades[0] = n;
        int i = 0;
        while (n != 0 && fillGrades.length != 10) {
            n = scanner.nextInt();
            if (n == 0) break;
            int[] tempArray = new int[fillGrades.length + 1];
            System.arraycopy(fillGrades, 0, tempArray, 0, fillGrades.length);
            tempArray[1 + i] = n;
            fillGrades = tempArray;
            i++;
        }
        setGrades(fillGrades);
        return fillGrades;
    }
    /**
     * Расчет среднего балла
     *@return
     */
    public double averageScore() {
        int sum = 0;
        for (int i = 0; i < this.grades.length; i++) {
            sum += grades[i];
        }
        return (int) (sum * 10 / grades.length) / 10.0;
    }
    /**
     * Метод добавления новой оценки
     * @return
     */
    public int[] addScore() {
        // если количество оценок менее 10
        Scanner in = new Scanner(System.in);
        System.out.print("Введите оценку: ");
        int[] tempArray;
        if (this.grades.length + 1 < 10) {
            tempArray = new int[this.grades.length + 1];
            System.arraycopy(this.grades, 0, tempArray, 0, this.grades.length);
            tempArray[tempArray.length - 1] = in.nextInt();

        } else {
            tempArray = new int[this.grades.length];
            System.arraycopy(this.grades, 1, tempArray, 0, 9);
            tempArray[tempArray.length - 1] = in.nextInt();
        }
        grades = tempArray;
        setGrades(this.grades);
        return this.grades;
    }
}

