package HomeWork.HW_3_1.StudentService;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Кратос", "Годофварович",new int[]{5, 5, 4, 3, 5});
        Student student2 = new Student("Макл", "Морхейм", new int[]{5, 5, 5, 3, 5, 5});
        Student student3 = new Student("Уил", "Байерс", new int[]{4, 4, 5, 4, 5, 4});;

        System.out.println("оценки 1 студента" + Arrays.toString(student1.getGrades()));
        student1.addScore();
        System.out.println("оценки 2 студента" + Arrays.toString(student2.getGrades()));
        student2.addScore();
        System.out.println("оценки 3 студента" + Arrays.toString(student3.getGrades()));
        student3.addScore();
    }
}
