package HomeWork.HW_3_1.StudentService;

import java.util.Arrays;

public class StudentService {
    public static Student bestStudent(Student[] students) {
        double temp = students[0].averageScore();
        int index = 0;

        for (int i = 1; i < students.length; i++) {
            if  (temp < students[i].averageScore()) {
                temp = students[i].averageScore();
                index = i;
            }
        }
        return students[index];
    }

    public static void sortBySurname(Student[] students) {
        String[] surnames = new String[students.length];
        for (int i = 0; i < surnames.length; i++) {
            surnames[i] = students[i].getSurname();
        }
        Arrays.sort(surnames);
        Student temp;

        for (int i = 0; i < surnames.length; i++) {
            for (int j = 0; j < surnames.length; j++) {
                if (surnames[i].equals(students[j].getSurname())) {
                    temp = students[i];
                    students[i] = students[j];
                    students[j] = temp;
                    break;
                }
            }
        }
    }


}
