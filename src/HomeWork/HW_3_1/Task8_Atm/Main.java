package HomeWork.HW_3_1.Task8_Atm;

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm(65, 0.015384615);
        System.out.println("Переводим 300 долларов в рубли: " + atm1.getRoublesFromDollars(300));
        System.out.println("Переводим 80 000 рублей в доллары: " + atm1.getDollarsFromRoubles(80000));
        Atm atm2 = new Atm(6.30, 0.1587301587);
        System.out.println("Переводим 300 долларов в рубли: " + atm2.getRoublesFromDollars(300));
        System.out.println("Переводим 80 000 рублей в доллары: " + atm2.getDollarsFromRoubles(80000));
        System.out.println("Количество операций:" + Atm.getCounter());
    }
}
