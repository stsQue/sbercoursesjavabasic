package HomeWork.HW_3_1.Task8_Atm;

public class Atm {
    private double dollarToRoubleRating;
    private double roubleToDollarRating;
    private static int counter = 0;

     /**
     * Конструктор с проверкой на положительные значения
     * @param dollarToRoubleRating
     * @param roubleToDollarRating
     */
    Atm (double dollarToRoubleRating, double roubleToDollarRating) {
        if (dollarToRoubleRating > 0 && roubleToDollarRating > 0) {
            this.dollarToRoubleRating = dollarToRoubleRating;
            this.roubleToDollarRating = roubleToDollarRating;
            counter++;
        } else {
            throw new IllegalArgumentException("Введенные значения должны быть положительными.");
        }
    }

    public double getRoublesFromDollars (double sum) {
        sum = (int) (this.dollarToRoubleRating * sum * 100) / 100.00;
        counter++;
        return sum;

    }
    public double getDollarsFromRoubles (double sum) {
        sum = (int)(this.roubleToDollarRating * sum * 100) / 100.00;
        counter++;
        return sum;
    }

    public static int getCounter() {
        return counter;
    }
}
