package HomeWork.HW_3_1.Cat;

import java.util.Random;

/*
Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */
public class Cat {
    // закрытые методы класса Cat
    private void sleep() {
        System.out.println("sleep");
    }
    private void meow (){
        System.out.println("meow");
    }
    private void eat () {
        System.out.println("eat");
    }
    // открытый метод вызова статуса
    public void getStatus () {
        Random generator = new Random();
        switch (generator.nextInt(3)) {
            case 0 -> eat();
            case 1 -> sleep();
            case 2 -> meow();
        }
    }
}

