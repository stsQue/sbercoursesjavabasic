package HomeWork.HW_3_1.Task5_DayOfWeek;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */

public class DayOfWeek {
    private static byte[] dayNumber = {1, 2, 3, 4, 5, 6, 7};
    private static String[] dayName = {"понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"};
    private DayOfWeek(){}

    public static String[] week() {
        String[] week = new String[7];
        for (byte i = 0; i < 7; i++) {
            week[i] = dayNumber[i] +" " + dayName[i];
        }
        return week;
    }

}
