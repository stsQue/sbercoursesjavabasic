package HomeWork.HW_3_1.Task6_AmazingString;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        AmazingString amazingString = new AmazingString(new char[]{' ', 'm', 'o', 'r', 'n', 'i', 'n', 'g'});
        System.out.println("Incoming array: " + Arrays.toString(new char[]{' ', 'm', 'o', 'r', 'n', 'i', 'n', 'g'}));
        System.out.println("Char to string: " + amazingString.charArrayToString());
        System.out.println("amazingString getChar(2): " + amazingString.getCharFromString(2));
        System.out.println("amazingString length(): " + amazingString.length());
        amazingString.writeLine();
        System.out.println("Str has sbstr: " + amazingString.hasSubstring("nin"));
        System.out.println("Str has charArray: " + amazingString.hasSubstring(new char[]{'n', 'i', 'n'}));
        System.out.println("Delete 1st space: " + amazingString.deleteFirstWhiteSpace());
        System.out.println("Reverse string: " + amazingString.reverse());
    }
}
