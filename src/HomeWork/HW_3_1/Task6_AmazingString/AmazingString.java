package HomeWork.HW_3_1.Task6_AmazingString;

/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.
● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */
public class AmazingString {
    private char[] ch;
    private String str;

    AmazingString(char[] ch) {
        this.ch = ch;
    }

    AmazingString(String str) {
        this.str = str;
    }

    /**
     * Массив чаров в строку
     * @return
     */
    String charArrayToString() {
        StringBuilder str = new StringBuilder(new String());
        for (int i = 0; i < ch.length; i++) {
            str.append(this.ch[i]);
        }
        this.str = str.toString();
        return this.str;
    }

    /**
     *Вернуть i-ый символ строки
     */
    String getCharFromString (int index) {
        StringBuilder sb = new StringBuilder(this.str);
        if (index >= 0 && index < sb.length() - 1) {
            return sb.substring(index, index + 1).toString();
        } else {
            return sb.substring(index - 1, index).toString();
        }
    }

    /**
     * Вернуть длину строки
     * @return
     */
    int length () {
        int count = 0;
        char[] chars = this.str.toCharArray();
        for (char e: chars) {
            count++;
        }
        return count;
    }
    /**
     * Вывести строку на экран
     */
    public void writeLine () { //привет c#
        System.out.println(this.str);
    }

    /**
     * Содержит ли строка подстроку
     */
    boolean hasSubstring (String string) {
        return this.str.matches(".*\\Q" + string + "\\E.*");
    }
    //можно перевести обе строки в массивы и последовательно проверять, начиная с первого символа подстроки
    // но это много кода и некрасиво
    boolean hasSubstring (char[] charArray) {
        AmazingString charArrayToString = new AmazingString(charArray);
        return this.str.matches(".*\\Q" + charArrayToString.charArrayToString() + "\\E.*");
    }

    //Удалить из строки AmazingString ведущие пробельные символы, если
    //они есть
    public String deleteFirstWhiteSpace() {
        AmazingString string = new AmazingString(this.str);
        if (string.hasSubstring(" ")) {
            this.str = this.str.replaceAll("^\\s","");
        }
        return this.str;
    }

    //Развернуть строку (первый символ должен стать последним, а
    //последний первым и т.д.)
    public String reverse () {
        StringBuilder reverseString = new StringBuilder(this.str);
        return reverseString.reverse().toString();
    }


}
