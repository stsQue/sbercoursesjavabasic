package HomeWork.HW_1_3;

import java.util.Scanner;

/*
В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)

Ограничения:
0 < n < 1000000
 */
public class Solution6 {
    static final int EIGHT = 8, FOUR = 4, TWO = 2, ONE = 1;
    static void CountBanknotes (int n) {
        int countEight = 0, countFour = 0, countTwo = 0, countOne = 0;
        while (n > 0) {
            if (n >= EIGHT) {
                countEight = n / EIGHT;
                n = n % EIGHT;
            } else if (n >= FOUR) {
                countFour = n / FOUR;
                n = n % FOUR;
            } else if (n >= 2) {
                countTwo = n / TWO;
                n = n % TWO;
            } else if (n >= 1) {
                countOne = n / ONE;
                n = n % ONE;
            }
        }
        System.out.println(countEight + " " + countFour + " " + countTwo + " " + countOne);
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        CountBanknotes(input.nextInt());

    }
}
