package HomeWork.HW_1_3;
/*
На вход подается два положительных числа m и n. Необходимо вычислить m^1
+ m^2 + ... + m^n

Ограничения:
0 < m, n < 10
 */
import java.util.*;

public class Solution3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt(), n = input.nextInt();
        int result = 0;

        for (int i = n; i > 0; i--) {
        result += Math.pow(m, i);
        }
        System.out.println(result);
    }
}
