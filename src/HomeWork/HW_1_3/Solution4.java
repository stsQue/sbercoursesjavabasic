package HomeWork.HW_1_3;

/*
Дано натуральное число n. Вывести его цифры в “столбик”.
Ограничения:
0 < n < 1000000
 */

import java.util.*;

public class Solution4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        // счтаем количество разрядов во введенном числе
        int d = 1;
        for (int i = 1; i < (int) (Math.log10(n) + 1); i++) {
            d *= 10;
        }

        // выводим результат
        int result;

        for (; d  >= 1 ; d = d / 10) {
            if (d > 1) {
                result = n / d;
                n -= d * result;
                System.out.println(result);

            } else if (d == 1) {
                System.out.println(n % 10);
                break;
            }

        }
}
    }
