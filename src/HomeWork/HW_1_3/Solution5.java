package HomeWork.HW_1_3;
/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения:
0 < m, n < 10
 */

import java.util.Scanner;

public class Solution5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt(), n = input.nextInt();
        int d = 0;

        do{
            d +=n;
        } while (d <= m);

        System.out.println(m - d + n);
    }
}
