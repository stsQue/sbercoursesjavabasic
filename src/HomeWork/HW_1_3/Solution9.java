package HomeWork.HW_1_3;
/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.

Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.

Ограничения:
0 < n < 1000
-1000 < ai < 1000
 */
import java.util.Scanner;

public class Solution9 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();

        int[] arr = new int[str.split(" ").length];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(str.split(" ")[i]);
        }

        int minus = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                minus++;
            } else {
                break;
            }
        }
        System.out.println(minus);
    }
}
