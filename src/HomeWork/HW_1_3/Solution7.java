package HomeWork.HW_1_3;
/*
Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
Ограничения:
0 < s.length() < 1000
 */

import java.util.Scanner;

public class Solution7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();

        int spaceCount = s.length() - s.replaceAll(" ", "").length();
        System.out.println(s.length() - spaceCount);
    }

}
