package HomeWork.HW_1_3;

import java.util.Scanner;

/*
2. Сумма чисел
На вход подается два положительных числа m и n.
Найти сумму чисел между m и n включительно.

Ограничение:
0 < m, n < 10
m < n

Пример входных данных
7 9
Пример выходных данных
24

 */
public class Solution2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m = in.nextInt(), n = in.nextInt();
        int sum = 0;

        for (int i = m; i <= n; i++) {
            sum +=i;
        }
        System.out.println(sum);
    }
}
