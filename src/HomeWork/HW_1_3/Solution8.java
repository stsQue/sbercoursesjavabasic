package HomeWork.HW_1_3;
/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.

Ограничения:
0 < m, n, ai < 1000
 */

import java.util.Scanner;

public class Solution8 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int p = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > p) {
                sum += arr[i];
            }
        }
        System.out.println(sum);
    }
}
