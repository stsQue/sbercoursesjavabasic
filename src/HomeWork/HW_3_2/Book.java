package HomeWork.HW_3_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Book {
    private String bookName;
    private String author;
    private Visitor visitor = null;
    private int[] scoreArray = new int[0];

    public int[] getScoreArray() {
        return scoreArray;
    }

    public void setScoreArray(int[] scoreArray) {
        this.scoreArray = scoreArray;
    }

    Book (){}
    public Book(String bookName, String author) {
        this.bookName = bookName;
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Visitor getVisitor() {
        return visitor;
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    List<Book> bookArray = new ArrayList<>();

    /**
     * Инициализируем готовый список книг в библиотеку
     */
    public void nameBook() {
        bookArray.add(new Book("Бородино", "М.Лермонтов"));
        bookArray.add(new Book("Мастер и Маргарита", "М.Булгаков"));
        bookArray.add(new Book("Идиот", "Ф.Достоевский"));
        bookArray.add(new Book("Преступление и наказание", "Ф.Достоевский"));
        bookArray.add(new Book("А зори здесь тихие", "Б.Васильев"));
    }

    /**
     * Генератор случайных оценок для объявления "имеющихся данных"
     */
    public void ratingBookGenerator (){
        Random generator = new Random();
        for (int i = 0; i < bookArray.size(); i++) {
            bookArray.get(0).setScoreArray(new int[]{generator.nextInt(1, 6), generator.nextInt(1, 6), generator.nextInt(1, 6), generator.nextInt(1, 6), generator.nextInt(1, 6)});
        }
    }

}
