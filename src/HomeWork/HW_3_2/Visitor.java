package HomeWork.HW_3_2;

import java.util.ArrayList;
import java.util.List;

public class Visitor {
    // имя читателя
    private String name;
    //ID читателя
    private static int id;
    //Статус книги
    private Book book = null;



    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public Book getBook() {
        return book;
    }
    public void setBook(Book book) {
        this.book = book;
    }

    public int generateID () {
        return ++id;
    }
    Visitor(){}
    Visitor(String name) {
        this.name = name;
    }
    List<Visitor> visitorArray = new ArrayList<>();



}
