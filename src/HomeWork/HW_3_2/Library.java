package HomeWork.HW_3_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {

    Library() {
    }

    Visitor visitor = new Visitor();
    Book book = new Book();

    /**
     * Добавление нового читателя в массив читателей
     *
     * @param visitorName
     */
    public void addVisitorArrays(Visitor visitorName) {
        visitor.generateID();
        visitor.visitorArray.add(visitorName);
    }

    /**
     * Добавление книги в библиотеку
     *
     * @param bookName
     */
    public void addBook(Book bookName) {
        for (Book e : book.bookArray) {
            if (bookName.getBookName().equals(e.getBookName()) && bookName.getAuthor().equals(e.getAuthor())) {
                System.out.println("Книга не может быть добавлена, эта уже есть в библиотеке.");
                return;
            }
        }
        book.bookArray.add(bookName);
    }

    /**
     * Удаление книги из библиотеки
     *
     * @param bookName
     */
    public void deleteBook(Book bookName) {
        boolean a = false, b = false;
        for (Book e : book.bookArray) {
            if (bookName.getBookName().equals(e.getBookName()) && bookName.getAuthor().equals(e.getAuthor())) {
                a = true;
                if (bookName.getVisitor() == null) b = true;
            }
        }
        if (a && b) {
            System.out.printf("Книга %s удалена \n", bookName.getBookName());
            book.bookArray.remove(bookName);
        } else if (!a) {
            System.out.println("Такой книги нет в библиотеке.");
        } else {
            System.out.println("Книга находится у читателя.");
        }
    }

    /**
     * Выдача читателю книги
     *
     * @param visitorName
     * @param bookName
     */
    public void takeBook(Visitor visitorName, Book bookName) {
        boolean isLibrary = false, visitorHaveNotBook = false, statusBook = false;
        for (Book e : book.bookArray) {
            if (bookName.getBookName().equals(e.getBookName()) && bookName.getAuthor().equals(e.getAuthor())) {
                isLibrary = true;
            }
        }
        for (Visitor e : visitor.visitorArray) {
            if (visitorName.getId() != 0 && visitorName.getBook() == null) {
                visitorHaveNotBook = true;
            }
        }
        for (Book e : book.bookArray) {
            if (bookName.getVisitor() == null) {
                statusBook = true;
            }
        }
        if (isLibrary && visitorHaveNotBook && statusBook) {
            visitorName.setBook(bookName);
            bookName.setVisitor(visitorName);
        } else if (!isLibrary) {
            System.out.println("Такой униги нет в библиотеке");
        } else if (!visitorHaveNotBook) {
            System.out.println("Книга находится у посетителя");
        } else {
            System.out.println("Книга в данный момент одолжена");
        }
    }

    /**
     * Возвращает книгу по названию
     *
     * @param bookName
     * @return
     */
    public Book findBook(Book bookName) {
        for (Book el : book.bookArray) {
            if (bookName.getBookName().equals(el.getBookName())) {
                System.out.println(bookName.getBookName() + "" + bookName.getAuthor());
                return book;
            }
        }
        System.out.println("Такой книги нет в библиотеке.");
        return book;
    }

    /**
     * Сдать книгу в библиотеку
     *
     * @param bookName
     * @param visitorName
     */
    public void returnBook(Book bookName, Visitor visitorName) {
        for (Book el : book.bookArray) {
            if (bookName.getBookName().equals(el.getBookName())) {
                bookName.setVisitor(null);
            }
        }
        for (Visitor el : visitor.visitorArray) {
            if (visitorName.getId() == el.getId()) {
                visitorName.setBook(null);
            }
        }
    }

    /**
     * Поиск книг по автору
     *
     * @param bookAuthor
     * @return
     */
    public List<Book> booksByAthor(String bookAuthor) {
        List<Book> booksByAthor = new ArrayList();
        for (Book el : book.bookArray) {
            if (bookAuthor.equals(el.getAuthor())) {
                booksByAthor.add(el);
                System.out.println(el.getBookName() + " " + el.getAuthor());
            }
        }
        return booksByAthor;
    }

    /**
     * Метод добавления оценок
     * @param bookName
     * @return averageScore
     */

    public double addElementScoreArrayAndReturnGrade(Book bookName) {
        Scanner in = new Scanner(System.in);
        double sum = 0, averageScore = 0;
        String inputMessage = "Введите оценку книги: ";
        int[] temp;
        for (Book el : book.bookArray) {
            if (bookName.getBookName().equals(el.getBookName())) {
                if (el.getScoreArray().length == 0) {
                    System.out.print(inputMessage);
                    temp = (new int[]{in.nextInt()});
                    el.setScoreArray(temp);
                } else {
                    temp = new int[el.getScoreArray().length + 1];
                    System.arraycopy(el.getScoreArray(), 0, temp, 0, el.getScoreArray().length);
                    System.out.print(inputMessage);
                    temp[temp.length - 1] = in.nextInt();
                    el.setScoreArray(temp);
                }
                    for (int i = 0; i < temp.length; i++) {
                        sum += temp[i];
                    }

                averageScore = ((int) ((sum / temp.length) * 10) / 10.0);

            }
        }
        System.out.println(averageScore);
        return averageScore;
    }
}







