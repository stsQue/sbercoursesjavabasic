package HomeWork.HW_3_2;

public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        // получаем имеющиеся книги в библоитеке
        library.book.nameBook();

        // принимаем информацию о читателе
        Visitor visitor1 = new Visitor("Нейт Анчартедович");
        Visitor visitor2 = new Visitor("Билл Гейтс");
        Visitor visitor3 = new Visitor("Геннадий П.");
       // добавляем читателя в список читателей бибилотеки
        library.addVisitorArrays(visitor1);
        library.addVisitorArrays(visitor2);
        library.addVisitorArrays(visitor3);


        // выводим инфу о добавленном читателе и его ID
        System.out.println("Имя читателя: " + visitor3.getName() + "\n" + "ID-пользователя:  " + visitor3.getId());
        // новые объекты - новая книга и книга которая уже есть в бибилиотеке
        Book book0 = new Book("Java Полное руководство десятое издание", "Герберт Шилдт");
        Book book1 = new Book("Бородино", "М.Лермонтов");
        Book book2 = new Book ("А зори здесь тихие", "Б.Васильев");
        // Найти и вернуть книгу по названию
        library.findBook(book1);
        // проверка метода добавления книги в библиотеку
        library.addBook(book0);
        library.addBook(book1);
        // удаляем книгу
        library.deleteBook(book1);

        // найти и вернуть список книг по автору
        library.booksByAthor("Ф.Достоевский");

        // Одолжить книгу посетителю по названию
        library.takeBook(visitor3, book0);
        System.out.println(book0.getBookName() + " выдана " + book0.getVisitor().getName());
        // Вернуть книгу в библиотеку от посетителя
        library.returnBook(book0, visitor3);
        System.out.println(book0.getVisitor() + "  " + visitor3.getBook());

        // Добавление рейтинга и выведение среднего арифметического
        System.out.println("Первая книга");
        library.addElementScoreArrayAndReturnGrade(book0);
        library.addElementScoreArrayAndReturnGrade(book0);
        library.addElementScoreArrayAndReturnGrade(book0);
        library.addElementScoreArrayAndReturnGrade(book0);
        System.out.println("Вторая книга");
        library.addElementScoreArrayAndReturnGrade(book2);
        library.addElementScoreArrayAndReturnGrade(book2);
        library.addElementScoreArrayAndReturnGrade(book2);

        }
    }

