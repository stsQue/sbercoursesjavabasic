//package HomeWork.HW_2_2;

import java.util.Scanner;

/*
Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
хранения участников и оценок неудобная, а победителя определить надо. В
первой таблице в системе хранятся имена хозяев, во второй - клички животных,
в третьей — оценки трех судей за выступление каждой собаки. Таблицы
связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
помочь Пете определить топ 3 победителей конкурса.

На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.

Гарантируется, что среднее арифметическое для всех участников будет
различным.
 */
public class Solution7 {
    public static void main(String[] args) {
        // вводим количество участников, создаем и заполняем массивы имя хозяев, имя питомцев, очки
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();

        String[] name = new String[number];
        for (int i = 0; i < number; i++) {
            name[i] = in.next();
        }

        String[] pet = new String[number];
        for (int i = 0; i < number; i++) {
            pet[i] = in.next();
        }

        int[][] scores = new int[number][3];
        for (int i = 0; i < scores.length; i++) {
            for (int j = 0; j < scores[i].length; j++) {
                scores[i][j] = in.nextInt();
            }
        }

        // составляем массив среднего балла
        double[] averageScores = new double[number];
        averageScores = getTotalScores(scores, averageScores);

        // сортируем данные
        double temp = 0;
        int count = -1;
        String tempName, tempPet;
        do {
            count = 0;
            for (int i = 0; i < averageScores.length - 1; i++) {
                if (averageScores[i] > averageScores[i + 1]) {
                    temp = averageScores[i + 1];
                    tempName = name[i + 1];
                    tempPet = pet[i + 1];
                    averageScores[i + 1] = averageScores[i];
                    name[i + 1] = name[i];
                    pet[i + 1] = pet[i];
                    averageScores[i] = temp;
                    name[i] = tempName;
                    pet[i] = tempName;
                    count++;
                }
            }
        } while (count > 0);
        for (int i = averageScores.length - 1; i > averageScores.length - 4 ; i--) {
            System.out.println(name[i] + ": " + pet[i] + ", " + averageScores[i]);
        }
    }

    /**
     * метод расчета среднего балла
     *
     * @param scores
     * @param totalScores
     * @return
     */
    private static double[] getTotalScores(int[][] scores, double[] totalScores) {
        int sum = 0;
        for (int i = 0; i < scores.length; i++) {
            sum = 0;
            for (int j = 0; j < scores[i].length; j++) {
                sum += scores[i][j];
            }
            totalScores[i] = (int) (sum * 10 / scores[i].length) / 10.0;
        }
        return totalScores;
    }

    /**
     * Метод заполнения массива строк
     *
     * @param array
     * @return
     */
    public static String[] setArray(String[] array) {
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            array[i] = in.next();
        }
        return array;
    }

    /**
     * Метод заполнения массива инт
     *
     * @param array
     * @return
     */
    public static int[][] setArray(int[][] array) {
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = in.nextInt();
            }
        }
        return array;
    }
}
