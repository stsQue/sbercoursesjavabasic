package HomeWork.HW_2_2;

import java.util.Scanner;
/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.

Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.

Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.

Ограничения:
● 0 < N < 100
● 0 < ai < 1000
 */
public class Solution5 {

    static boolean isSymmetric(int mat[][], int size) {
        int j = 0;
        for (int i = size - 1; i - 1 >= 0; i--) {
            if (mat[i - 1][j] != mat[i][j + 1]) return false;
            j++;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n= scanner.nextInt();
        int[][] number = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                number[i][j] = scanner.nextInt();
            }
        }
        System.out.println(isSymmetric(number, n));
    }
}