package HomeWork.HW_2_2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.

Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
 */
public class Solution4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // создаем массив размерности size
        int size = input.nextInt();
        int[][] arr = new int[size][size];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = input.nextInt();
            }
        }
        // получаем координаты элемента P
        int findValue = input.nextInt();

        // поиск элемента и получение его координат
        int raw = 0, column = 0;
        {
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[i].length; j++) {
                    if (arr[i][j] == findValue) {
                        raw = i;
                        column = j;
                    }
                }
            }
        }

        // создаем новый массив
        int[][] arr2 = new int[size - 1][size - 1];
        // заполняем массив
        int arr2i = 0, arr2j = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if(i != raw) {
                    if (j != column) {
                        arr2[arr2i][arr2j] = arr[i][j];
                        ++arr2j;
                    }
                } else {
                    --arr2i;
                    break;
                }
            }
            ++arr2i;
            arr2j = 0;
        }
        // выводим результат
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j <arr2[i].length ; j++) {
                if (j != arr2[i].length - 1) {
                    System.out.print(arr2[i][j] + " ");
                } else {
                    System.out.print(arr2[i][j]);
                }
            }
            System.out.println();
        }
    }
}

