//package HomeWork.HW_2_2;

import java.util.Scanner;

public class Solution3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String[][] arr = new String[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = "0";
            }
        }
        int xColumn = in.nextInt();
        int yRaw = in.nextInt();

        arr[yRaw][xColumn] = "K";
        if (yRaw + 2 < n) {
            if (xColumn + 1 < n) arr[yRaw + 2][xColumn + 1] = "X";
            if (xColumn - 1 >= 0) arr[yRaw + 2][xColumn - 1] = "X";
        }
        if (yRaw - 2 >= 0) {
            if (xColumn + 1 < n) arr[yRaw - 2][xColumn + 1] = "X";
            if (xColumn - 1 >= 0) arr[yRaw - 2][xColumn - 1] = "X";
        }
        if (xColumn + 2 < n) {
            if (yRaw + 1 < n) arr[yRaw + 1][xColumn + 2] = "X";
            if (yRaw - 1 >= 0) arr[yRaw - 1][xColumn + 2] = "X";
        }
        if (xColumn - 2 >= 0) {
            if (yRaw + 1 < n) arr[yRaw + 1][xColumn - 2] = "X";
            if (yRaw - 1 >= 0) arr[yRaw - 1][xColumn - 2] = "X";
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j != n - 1)
                    System.out.print(arr[i][j] + " ");
                else System.out.print(arr[i][j]);

            }
            System.out.println();

        }

    }
}

