//package HomeWork.HW_2_2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
 */
public class Solution8 {
    public static void main(String[] arg) {

        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        int sum = recursion(x);
        System.out.println(sum);

    }

    public static int recursion(int sum) {
        if (sum / 10 >= 1) {
            int tempvar = sum % 10;
            int remain = sum / 10;
            return tempvar + recursion(remain);
        } else {
            return sum;
        }

    }
}
