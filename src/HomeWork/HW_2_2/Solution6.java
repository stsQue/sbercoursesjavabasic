//package HomeWork.HW_2_2;

import java.util.Scanner;

/*
 Петя решил начать следить за своей фигурой. Но все существующие
приложения для подсчета калорий ему не понравились и он решил написать
свое. Петя хочет каждый день записывать сколько белков, жиров, углеводов и
калорий он съел, а в конце недели приложение должно его уведомлять,
вписался ли он в свою норму или нет.

На вход подаются числа A — недельная норма белков, B — недельная норма
жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было
съедено Петей нутриентов в каждый день недели. Если за неделю в сумме по
каждому нутриенту не превышена недельная норма, то вывести “Отлично”,
иначе вывести “Нужно есть поменьше”.
 */
public class Solution6 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int[] norma = new int[4];
        int[][] weekKal = new int[7][4];
        // заполняем массив нормы
        for (int i = 0; i < norma.length; i++) {
            norma[i] = in.nextInt();
        }
        // заполняем массив калорий за неделю
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                weekKal[i][j] = in.nextInt();
            }
        }

        //считаем сумму за неделю и заполняем массив 3
        int[] totalWeek = new int[4];
        int sum = 0;
        for (int i = 0; i < 4; i++) {
            sum = 0;
            for (int j = 0; j < 7; j++) {
                sum +=weekKal[j][i];
            }
            totalWeek[i] = sum;
        }

        //сравниваем результаты с нормой
       boolean isMatch = isMatch(totalWeek, norma);
        if (isMatch) {
            System.out.println("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
    public static boolean isMatch(int[] arr1, int[] arr2) {
        int count = 0;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] <= arr2[i]) count++;
        }
        if (count == arr1.length) return true;
        else return false;
    }
}
