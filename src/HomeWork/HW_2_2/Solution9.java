package HomeWork.HW_2_2;
/*
На вход подается число N. Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.
 */
import java.util.Scanner;

public class Solution9 {
    public static String recursion(int n) {
        // Базовый случай
        if (n < 10) {
            return Integer.toString(n);
        } // Шаг рекурсии / рекурсивное условие
        else {
            return recursion(n / 10) + " " + n % 10;
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(recursion(in.nextInt())); // вызов рекурсивной функции
    }
}

