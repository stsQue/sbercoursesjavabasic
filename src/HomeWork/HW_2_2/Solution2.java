//ackage HomeWork.HW_2_2;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.

Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.

Ограничения:
● 0 < N < 100
● 0 <= X1, Y1, X2, Y2 < N
● X1 < X2
● Y1 < Y2
 */
public class Solution2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // получаем кол-во столбцов и строк массива, создаем и заполняем массив заданными параметрами
        int columns = in.nextInt();
        int rows = columns;
        int[][] arr = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                arr[i][j] = 0;
            }
        }
        // получаем координаты фигуры
        int yColumn1 = in.nextInt();
        int xRaw1 = in.nextInt();
        int yColumn2 = in.nextInt();
        int xRaw2 = in.nextInt();
        
        // рисуем фигуру в - основание, стороны, нижняя часть
        // делаем вернхнюю и нижнюю грань равным 1
        for (int i = yColumn1; i <= yColumn2 ; i++) {
            arr[xRaw1][i] = 1;
            arr[xRaw2][i] = 1;
        }
        // делаем боковые грани равными 1
        for (int i = xRaw1; i <=xRaw2 ; i++) {
            arr[i][yColumn1] = 1;
            arr[i][yColumn2] = 1;
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (j != columns - 1)
                 System.out.print(arr[i][j] + " ");
                else System.out.print(arr[i][j]);
            }
            System.out.println();
        }
                
    }
}
