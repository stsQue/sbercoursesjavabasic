package HomeWork.HW_2_2;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.

Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
 */
public class Solution1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int column = in.nextInt();
        int row = in.nextInt();
        int[][] arr = new int[row][column];
        // arr.length - количество строк в массиве
        for (int i = 0; i < arr.length; i++) {
            // arr[i].length - количество столбцов в строке i
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = in.nextInt();
            }
        }
        for (int i = 0; i < arr.length; i++) {
            Arrays.sort(arr[i]);
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i][0] + " ");
        }

    }
}
