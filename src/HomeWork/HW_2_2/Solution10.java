package HomeWork.HW_2_2;

import java.util.Scanner;

/*
На вход подается число N. Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.
 */
public class Solution10 {
    public static int recursion(int n) {
        // Базовый случай
        if (n < 10) {
            return n;
        }// Шаг рекурсии / рекурсивное условие
        else {
            System.out.print(n % 10 + " ");
            return recursion(n / 10);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(recursion(in.nextInt())); // вызов рекурсивной функции
    }
}

