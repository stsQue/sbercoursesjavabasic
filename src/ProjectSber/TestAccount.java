package ProjectSber;

public class TestAccount {
    public static void main(String[] args) {
        // Создадим новый счет id 1122 и новый баланс 20 000 руб
        Account account = new Account(1122, 20000);

        // Присвоить всем счетам годовую процентную ставку 4,5%
        Account.setAnnualInterestRate(4.5);

        //Снять со счета 2500
        account.withdraw(2500);

        // Пополнить счет
        account.deposit(3000);

        // Выведем информацию
        System.out.println("Баланс равен " + account.getBalance() + " руб.");
        System.out.println("Ежемесячный процент равен " + account.getMonthlyInterest() + " руб.");
        System.out.println("Этот счет был создан " + account.getDateCreated());

    }
}
