package ProjectSber;

import java.util.Scanner;


public class ConvertAmount {
    static final double ROUBLES_PER_DOLLAR = 72.12;

    public static void main(String[] args) {
        int[] dollarsArray;
        double[] roublesArray;
        int n;
        int i;

        Scanner input = new Scanner(System.in);

        //Отобразить инструкцию
        instruct();

        // получать кол-во конвертаций до тех пор,
        // пока не введено корректное значение
        do {
            System.out.print("Введите корректное количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);

        // получить n сумм денег в американских долларах
        System.out.print("Введите " + n + " сумм денег в американских долларах через пробел: ");
        dollarsArray = new int[n];
        for (i = 0; i < n; ++i) {
            dollarsArray[i] = input.nextInt();
        }

        // Конвертировать сумму денег в рубли
        roublesArray = find_roubles(dollarsArray, n);

        // Отобразить сумму денег в российских рублях
        // с окр. в пользу покупателя
        System.out.println("\n  Сумма, RUB  Сумма, USD");
        for (i = 0; i < n; ++i) {
            System.out.println("\t\t" + dollarsArray[i] + "\t"
                    + (int)(roublesArray[i] * 100) / 100.0);
            
        }
    }
    /**
     * Инструкция
     */
    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег" +
                " из американских долларов в российские рубли.");
        System.out.println("Курс покупки равен " + ROUBLES_PER_DOLLAR +
                " рубля.\n");
    }
    /**
     * Конвертирует из USD в RUB
     */
    public static double[] find_roubles(int[] dollarsArray, int n) {
        double[] roublesArray = new double[n];
        int i;

        for (i = 0; i < n; i++) {
            roublesArray[i] = ROUBLES_PER_DOLLAR * dollarsArray[i];
        }
        return roublesArray;
    }
}