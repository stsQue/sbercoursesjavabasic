package week9.oop3.icecreamfactory;

public class Main {
    public static void main(String[] args) {
        IceCream cherryIceCream = IceCreamFactory.getIceCream(IceCreamType.CHERRY);
//        IceCream chocolate = IceCreamFactory.getIceCream(new ChocolateIceCream());
//        IceCream chocolate1 = new ChocolateIceCream();
        IceCream vanilla = IceCreamFactory.getIceCream(VanillaIceCream.class);
        
        cherryIceCream.printIngredients();
        vanilla.printIngredients();
        
//        XmlFile resultFile = XmlFileGeneratorFactory.create(fileDataSource);
    }
}
