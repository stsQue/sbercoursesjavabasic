package professional.week4.functional.task3;

/*
получить значение PI через наш интерфейс (функциональный)
 */
public class Main {
    public static void main(String[] args) {
        MyPiInterface ref = () -> Math.PI;
        System.out.println(ref.getPiValue());
        
        MyPiInterface ref2;
        ref2 = () -> 3.14;
        System.out.println(ref2.getPiValue());
    }
}
