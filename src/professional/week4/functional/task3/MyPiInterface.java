package professional.week4.functional.task3;

@FunctionalInterface
public interface MyPiInterface {
    double getPiValue();
}
