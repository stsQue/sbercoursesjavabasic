package professional.week4.functional.task4;

@FunctionalInterface
public interface ReverseInterface {
    String reverse(String n);
}
