package professional.week4.functional.task4;

import professional.week4.functional.task4.ReverseInterface;

/*
реализовать метод, чтобы вывести строку наоборот, используя наш ReverseInterface
 */
public class Main {
    public static void main(String[] args) {
        ReverseInterface reverseInterface = (str) -> {
            String res = "";
            for (int i = str.length() - 1; i >= 0; i--) {
                res += str.charAt(i);
            }
            return res;
        };
        ReverseInterface reverseInterface1 = s -> new StringBuilder(s).reverse().toString();
        
        System.out.println(reverseInterface.reverse("Lambda"));
        System.out.println(reverseInterface1.reverse("Lambda"));
    }
}
