package professional.TU.week4.functional.task5;

@FunctionalInterface
public interface MyGenericInterface<T> {
    T func(T t);
}
