package professional.week4.functional.task1;

//Создать таймер, который считает время выполнения метода, используя Runnable.
public class Main {
    public static void main(String[] args) {
        //обычный вызов (стандартный)
        Timer timer = new Timer();
        timer.measureTime(new SimpleSummator());
        System.out.println(timer.timeNanoseconds);
        
        //анонимным классом
        Timer timer1 = new Timer();
        timer1.measureTime(new Runnable() {
            @Override
            public void run() {
                long sum = 0;
                for (int i = 0; i <= 1_000_000_000; ++i) {
                    sum += i;
                }
                System.out.println(sum);
            }
        });
        System.out.println(timer1.timeNanoseconds);
        
        //java 8 - lambda
        Timer timer2 = new Timer();
        timer2.measureTime(() -> {
            long sum = 0;
            for (int i = 0; i <= 1_000_000_000; ++i) {
                sum += i;
            }
            System.out.println(sum);
        });
        System.out.println(timer2.timeNanoseconds);
    }
}
