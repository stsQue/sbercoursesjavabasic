package professional.week4.functional.task1;

public class Timer {
    
    public long timeNanoseconds = 0;
    
    public void measureTime(Runnable runnable) {
        long startTime = System.nanoTime();
        runnable.run();
        timeNanoseconds += System.nanoTime() - startTime;
    }
}
