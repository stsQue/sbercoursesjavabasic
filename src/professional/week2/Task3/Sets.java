package professional.week2.Task3;
/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Sets {
    //Set набор уникальных данных
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set1.add(3);
        System.out.println(set1);

        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(2);
        set2.add(2);

        set1.retainAll(set2);

        // для каждого элемента типа интежер внутри коллекции сделай это
        for (Integer elem: set1) {
            System.out.println(elem);
        }
        // примеры на лямды
        // set1.forEach(e -> System.out.println(e));
        // set1.forEach(System.out::println);

        // полезный метод для пересечения
        Collections.disjoint(set1, set2);
        // возвращает тру или фалс если хотя бы 1 элемент одной коллекции есть в другой
    }
}
