package professional.week2.Task2;

public class Pair <T, U>{
    private T first;
    private U second;

    public T getFirst () {
        return this.first;
    }
    public U getSecond() {
        return this.second;
    }
    public void setFirst(T first) {
        this.first = first;
    }
    public void setSecond(U second) {
        this.second = second;
    }

    @Override
    public String toString(){
        return "First: " + first + "; " + "Second: " + second;
    }
}
