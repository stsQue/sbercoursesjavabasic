package professional.week2.Task7;

import java.util.HashMap;
import java.util.Map;

public class MapSave {
    public static void main(String[] args) {
        Map<Integer, String> hm1 = new HashMap<>();

        hm1.put(1, "first");
        hm1.put(2, "second");
        hm1.put(3, "third");

        System.out.println("Элементы HashMap: " + hm1);
        System.out.println("Значение элемента 2: " + hm1.get(2));
    }
}
