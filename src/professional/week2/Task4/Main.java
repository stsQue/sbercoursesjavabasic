package professional.week2.Task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Boolean> list1 = new ArrayList<>();
        list1.add(true);
        list1.add(true);
        list1.add(false);
        System.out.println(ListUtils.countIf(list1, true));
        System.out.println(ListUtils.countIf(list1, false));

        // поговорим про String pool
        //https://topjava.ru/blog/rukovodstvo-po-string-pool-v-java

    }
}
