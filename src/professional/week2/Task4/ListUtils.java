package professional.week2.Task4;
/*
  Реализовать метод, который считает количество элементов в переданном List
  (элемент передается на вход, посчитать количество таких элементов в list)
 */

import java.util.List;

public class ListUtils {

    public static <T> int countIf(List<T> from, T elem) {
        int counter = 0;
        for (T e: from) {
            if (e.equals(elem))
                counter ++;
        }
        return counter;
    }

    private ListUtils(){}
}
