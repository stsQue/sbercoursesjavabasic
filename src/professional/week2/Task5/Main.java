package professional.week2.Task5;

import java.util.HashSet;
import java.util.TreeSet;

import static professional.week2.Task5.ConvertHashSet.convertHashSet;

public class Main {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("World");
        set.add("java");
        set.add("learning");
        set.add("MY");
        set.add("HELLO");
        System.out.println("HashSet: ");
        for (String s: set) {
            System.out.println(s + " ");
        }
        System.out.println("\nTreeSet: ");
        TreeSet<String>  set1 = convertHashSet(set);
        for (String t: set1) {
            System.out.println(t + " ");
        }
        // сортировка в HashSet идет по хешу, а в TreeSet сортировка осуществляется лексиграфически
    }
}
