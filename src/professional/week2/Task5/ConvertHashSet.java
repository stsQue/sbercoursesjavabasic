package professional.week2.Task5;
/*
Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.
Про деревья:
 https://habr.com/ru/post/330644/
 Структуры данных в картинках:
  https://habr.com/ru/post/128017/
 */

import java.util.HashSet;
import java.util.TreeSet;

public class ConvertHashSet {
    public static <T> TreeSet<T> convertHashSet(HashSet<T> from) {
        TreeSet<T> toReturn = new TreeSet<>();

        for (T elem: from) {
            toReturn.add(elem);
        }
        // или так
        //toReturn.addAll(from);

        return toReturn;
    }
}
