package professional.HomeWork.HW2.Task3;

import java.util.HashSet;
import java.util.Set;

//Реализовать класс PowerfulSet, в котором должны быть следующие методы:
//a.
//public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
//пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
//Вернуть {1, 2}
//b.
//public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
//объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
//Вернуть {0, 1, 2, 3, 4}
//c.
//public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
//возвращает элементы первого набора без тех, которые находятся также
//и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
public class PowerfulSet {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        // Проверка
        Set<Integer> set3 = intersection (set1, set2);
        set3.forEach(e -> System.out.println(e));
        set3 = union(set1, set2);
        set3.forEach(e -> System.out.println(e));
        set3 = relativeComplement(set1, set2);
        set3.forEach(e -> System.out.println(e));



    }

    /**
     * Возвращает пересечение Set1 и Set2
     * @param set1
     * @param set2
     * @return
     * @param <T>
     */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> intersection = new HashSet<>(set1);
        intersection.retainAll(set2);
        return intersection;
    }

    /**
     * Возвращает объединение Set1 и Set2
     * @param set1
     * @param set2
     * @return
     * @param <T>
     */
    public static <T> Set<T> union(Set<T> set1, Set<T> set2){
        Set<T> union = new HashSet<>(set1);
        union.addAll(set2);
        return union;
    }

    /**
     * Возвращает элементы первого набора без тех, которые находятся также и во втором наборе
     * @param set1
     * @param set2
     * @return
     * @param <T>
     */
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> relativeComplement = new HashSet<>(set1);
        relativeComplement.removeAll(set2);
        return relativeComplement;
    }
}
