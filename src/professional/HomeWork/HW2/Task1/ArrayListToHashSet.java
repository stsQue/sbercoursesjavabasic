package professional.HomeWork.HW2.Task1;

import java.util.ArrayList;
import java.util.HashSet;

// Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
// набор уникальных элементов этого массива. Решить используя коллекции.
public class ArrayListToHashSet {
    public static void main(String[] args) {
        ArrayList<String> setIn = new ArrayList<>();
        setIn.add("Hello");
        setIn.add("Hello");
        setIn.add("Hello");
        setIn.add("Mr.Neo");
        setIn.add("follow the");
        setIn.add("follow the");
        setIn.add("white");
        setIn.add("rabbit");

        System.out.println("\nsetResult: ");
        HashSet<String> setResult = uniqueValue(setIn);
        for (String t: setResult) {
            System.out.println(t + " ");
        }
    }

    /**
     * Добавляет значения из ArrayList в HashSet, возвращает уникальные значения
     * @param from
     * @return
     * @param <T>
     */
    public static <T> HashSet<T> uniqueValue (ArrayList<T> from) {
        HashSet<T> toReturn = new HashSet<>();
        toReturn.addAll(from);
        return toReturn;
    }
}
