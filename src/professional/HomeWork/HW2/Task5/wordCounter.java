package professional.HomeWork.HW2.Task5;
// Реализовать метод, который принимает массив words и целое положительное число k.
// Необходимо вернуть k наиболее часто встречающихся слов..
// Результирующий массив должен быть отсортирован по убыванию частоты
// встречаемого слова. В случае одинакового количества частоты для слов, то
// отсортировать и выводить их по убыванию в лексикографическом порядке.
import java.util.*;

public class wordCounter {
    public static void main(String[] args) {
        String[] words = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};
        int k = 4;

        System.out.println(theWords(words, k));

    }

    public static List<String> theWords(String[] words, int k) {
        Map<String, Integer> map = new TreeMap<>();
        for (String el : words) {
            map.put(el, map.getOrDefault(el, 0) + 1);
        }

        List<Map.Entry<String, Integer>> arr = new ArrayList<>();
        for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
            arr.add(stringIntegerEntry);
        }
        Collections.sort(arr, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getValue().compareTo(a.getValue());
            }
        });

        List<String> list = new ArrayList<>();
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : arr) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        Iterator<Map.Entry<String, Integer>> iterator = sortedMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            list.add(entry.getKey());
        }
        List<String> list1 = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            list1.add(list.get(i));
        }
        return list1;
    }
}
