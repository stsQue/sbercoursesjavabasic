package professional.HomeWork.HW2.Task2;

import java.util.Arrays;
import java.util.HashMap;

// С консоли на вход подается две строки s и t. Необходимо вывести true, если
// одна строка является валидной анаграммой другой строки и false иначе.
// Анаграмма — это слово или фраза, образованная путем перестановки букв
// другого слова или фразы, обычно с использованием всех исходных букв ровно
// один раз.
public class CheckAnagram {
    public static void main(String[] args) {
        String str1 = "mr.Anderson, red or blue tablet?";
        String str2 = "blue or red tablet, mr.Anderson?";
        System.out.println(isAnagram(str1, str2));
        System.out.println((isAnagramCollection(str1, str2)));
    }

    /**
     * Метод преобразует строки как массив символов, сортирует и сравнивает результаты
     * @param string1
     * @param string2
     * @return
     */
    static boolean isAnagram(String string1, String string2) {
        if (string1.length() != string2.length()) {
            return false;
        }
        char[] a1 = string1.toCharArray();
        char[] a2 = string2.toCharArray();
        Arrays.sort(a1);
        Arrays.sort(a2);
        return Arrays.equals(a1, a2);
    }

    // Решение через коллекции
    public static boolean isAnagramCollection(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        HashMap<Character, Integer> s1 = new HashMap<>();
        for (char el : str1.toLowerCase().toCharArray()) {
            s1.put(el, s1.getOrDefault(el, 0) + 1);
        }

        HashMap<Character, Integer> s2 = new HashMap<>();
        for (char el : str2.toLowerCase().toCharArray()) {
            s2.put(el, s2.getOrDefault(el, 0) + 1);
        }
        return s1.equals(s2);
    }
}
