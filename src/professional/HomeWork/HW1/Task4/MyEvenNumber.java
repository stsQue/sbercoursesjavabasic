package professional.HomeWork.HW1.Task4;
// Создать класс MyEvenNumber, который хранит четное число int n. Используя
// исключения, запретить создание инстанса MyPrimeNumber с нечетным числом
public class MyEvenNumber {
    public static void main(String[] args) throws Exception {
        final int n = 2;
        MyPrimeNumber myPrimeNumber = new MyPrimeNumber(n);
    }
}
