package professional.HomeWork.HW1.Task4;

public class MyPrimeNumber {
    private int b;

    public MyPrimeNumber(){
    }

    public MyPrimeNumber(int b) throws Exception {
        if (b % 2 != 0) {
            throw new Exception("Нельзя создать инстанс класса MyPrimeNumber с нечетным числом");
        }
        this.b = b;
    }

    public int getB() {
        return b;
    }
}
