package professional.HomeWork.HW1.Task6;


import java.time.LocalDate;


public class FormValidator {

    public static void checkName(String str) throws InvalidNameException {
        if (!str.matches("^[A-Z][a-z]{1,19}$")) {
            throw new InvalidNameException();
        }
    }

    public static void checkBirthdate(String str) throws InvalidCheckBirthdateException {
        if (!str.matches("(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.((19|20)\\d\\d)")) {
            throw new InvalidCheckBirthdateException();
        }

        LocalDate localDate = LocalDate.of(Integer.parseInt(str.substring(6)), Integer.parseInt(str.substring(3, 5)),
                Integer.parseInt(str.substring(0, 2)));
        LocalDate toDay = LocalDate.now();

        if (toDay.getYear() < localDate.getYear()) {
            throw new InvalidCheckBirthdateException();
        }
        if (toDay.getYear() == localDate.getYear()) {
            if (toDay.getMonthValue() < localDate.getMonthValue()) {
                throw new InvalidCheckBirthdateException();
            } else if (toDay.getMonthValue() == localDate.getMonthValue() && toDay.getDayOfMonth() < localDate.getDayOfMonth()) {
                throw new InvalidCheckBirthdateException();
            }
        }
    }

    public static void checkGender(String str) throws InvalidCheckGenderException {
        try {
            Gender.valueOf(str);
        } catch (Exception e) {
            throw new InvalidCheckGenderException();
        }
    }

    public static void checkHeight(String str) throws InvalidCheckHeightException {
        try {
            double d = Double.parseDouble(str);
            if (d <= 0) {
                throw new InvalidCheckHeightException();
            }
        } catch (Exception e) {
            throw new InvalidCheckHeightException();
        }
    }
}

