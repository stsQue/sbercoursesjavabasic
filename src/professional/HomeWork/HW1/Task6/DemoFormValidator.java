package professional.HomeWork.HW1.Task6;
/* Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
очень зол и ему придется написать свои проверки, а также кидать исключения,
если проверка провалилась. Помогите Пете написать класс FormValidator со
статическими методами проверки. На вход всем методам подается String str.
a. public void checkName(String str) — длина имени должна быть от 2 до 20
символов, первая буква заглавная.
b. public void checkBirthdate(String str) — дата рождения должна быть не
раньше 01.01.1900 и не позже текущей даты.
c. public void checkGender(String str) — пол должен корректно матчится в
enum Gender, хранящий Male и Female значения.
d. public void checkHeight(String str) — рост должен быть положительным
числом и корректно конвертироваться в double.
*/
public class DemoFormValidator {
    public static void main(String[] args) {

        try {
            FormValidator.checkName("Ilya");
        } catch (InvalidNameException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkBirthdate("31.12.2022");
        } catch (InvalidCheckBirthdateException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkGender("MALE");
        } catch (InvalidCheckGenderException e) {
            System.out.println(e.getMessage());
        }

        try {
            FormValidator.checkHeight("-1");
        } catch (InvalidCheckHeightException e) {
            System.out.println(e.getMessage());
        }
    }
}
