package professional.HomeWork.HW1.Task8;
//На вход подается число n, массив целых чисел отсортированных по
//возрастанию длины n и число p. Необходимо найти индекс элемента массива
//равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
//вывести -1.
import java.util.Scanner;

public class BinarySearch {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = in.nextInt();
        }
        int p = in.nextInt();
        System.out.println(binarySearch(arr, p));
    }

    public static int binarySearch(int[] arr, int key) {
        int low = 0;
        int high = arr.length - 1;

        while (high > low) {
            int mid = (low + high) / 2;
            if (key < arr[mid])
                high = mid - 1;
            else if (key == arr[mid])
                return mid;
            else
                low = mid + 1;
        }
        return -1;
    }
}
