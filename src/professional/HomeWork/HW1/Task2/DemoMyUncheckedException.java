package professional.HomeWork.HW1.Task2;
// Создать собственное исключение MyCheckedException, являющееся проверяемым
public class DemoMyUncheckedException   {
    public static void main(String[] args) {

        try {
            int [] arr = new int[2];
            arr[2] = 5;
        }catch (ArrayIndexOutOfBoundsException e){
            throw new MyUncheckedException("Сработало исключение MyUncheckedException");
        }
        finally {
            System.out.println("Выход из программы");
        }
    }
}
