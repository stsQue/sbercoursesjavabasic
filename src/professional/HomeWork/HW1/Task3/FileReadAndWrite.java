package professional.HomeWork.HW1.Task3;
// Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
//./output.txt текст из input, где каждый латинский строчный символ заменен на
//соответствующий заглавный. Обязательно использование try с ресурсами.

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Locale;
import java.util.Scanner;

public class FileReadAndWrite {
    private static final String PKG_DIRECTORY = "D:\\JAVA\\JAVA_learn\\sbercoursesjavabasic\\src\\professional\\HomeWork\\HW1\\Task3";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";

    public static void main(String[] args) {
        try {
            readAndWriteFile();
        } catch (IOException e) {
            System.out.println("FileReadAndWrite#main!error " + e.getMessage());
        }
    }

    public static void readAndWriteFile() throws IOException {
        Scanner scanner = new Scanner(new File(PKG_DIRECTORY + "\\" + INPUT_FILE_NAME));
        Writer writer = new FileWriter(PKG_DIRECTORY + "\\" + OUTPUT_FILE_NAME);

        try (scanner; writer) {
            while (scanner.hasNext()) {

                String temp = scanner.nextLine();

                for (int i = 0; i < temp.length(); i++) {
                    if (temp.charAt(i) >= 97 && temp.charAt(i) <= 122) {
                        writer.write(temp.toUpperCase().charAt(i));
                    } else {
                        writer.write(temp.charAt(i));
                    }
                }
                writer.write("\n");
            }
        }
    }
}
