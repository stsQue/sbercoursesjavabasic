package professional.HomeWork.HW1.Task1.var1;
// Создать собственное исключение MyCheckedException, являющееся проверяемым
public class Main {
    public static void main(String[] args) throws CarIsNotReadyException {
        Car car = new Car ("BMW");
        car.setFuel(true);
        car.isTravel();
        car.setFuel(false);
        car.isTravel();

    }
}
