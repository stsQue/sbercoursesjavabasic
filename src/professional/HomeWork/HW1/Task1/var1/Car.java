package professional.HomeWork.HW1.Task1.var1;

public class Car {
    private String model;
    private boolean fuel;

    public Car (String model) {
        this.model = model;
    }

    public void setFuel(boolean fuel) {
        this.fuel = fuel;
    }

    public void isTravel () throws CarIsNotReadyException {
        if (fuel) {
            System.out.println("Бензин есть, поехали!");
        } else {
            throw new CarIsNotReadyException("Твоя " + model + " не ездит без бензина, надо было Теслу брать!" );
        }
    }
}
