package professional.HomeWork.HW1.Task1.var1;

public class CarIsNotReadyException extends Exception {
    public CarIsNotReadyException(String message) {
        super(message);
    }
}
