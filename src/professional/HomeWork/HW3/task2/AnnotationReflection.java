package professional.HomeWork.HW3.task2;


import professional.HomeWork.HW3.task1.isLike;

public class AnnotationReflection {
    public static void main(String[] args) {
    printClassIsLike(ClassIsLike.class);
    }

    public static void printClassIsLike(Class<?> cls) {
        if (!cls.isAnnotationPresent(isLike.class)) {
            return;
        }
        isLike isLike = cls.getAnnotation(isLike.class);
        System.out.println("Значение " + isLike.flag());
    }
}
