package professional.HomeWork.HW3.task5;

public class Task2Dop {
    public static void main(String[] args) {

        System.out.println(bracketSequence("{)(}"));

    }

    public static boolean bracketSequence(String str) {
        char[] arr = str.toCharArray();
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;

        if (str.isEmpty()) {
            return true;
        }
        for (char el : arr) {
            if (el == ')') {
                count1--;
            } else if (el == '(') {
                count1++;
            }
            if (el == '}') {
                count2--;
            } else if (el == '{') {
                count2++;
            }
            if (el == ']') {
                count3--;
            } else if (el == '[') {
                count3++;
            }

            if (count1 < 0 || count2 < 0 || count3 < 0) {
                return false;
            }
        }
        return count1 == 0 && count2 == 0 && count3 ==0;
    }
}