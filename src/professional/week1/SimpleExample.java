package professional.week1;

import java.util.Scanner;

// схема всех исключений
// https://javastudy.ru/wp-content/uploads/2016/01/exceptionsInJavaHierarchy.png
// stacktrace - трассировка программы говорящей об ошибке

public class SimpleExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        System.out.println(100 / n);
        try {
            toDivide(100, n);
        }
        catch (ArithmeticException e) {
            //throw new My
            //throw new UnsupportedOperationException();
            // System.out.println("Делить на ноль нельзя");
            // System.out.println(e.getMessage());
        }
    }

    public static void toDivide (int a, int b) {
        System.out.println(a / b);
    }
}
