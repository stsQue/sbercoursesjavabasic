package professional.week3.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
Задача 5
Написать аннотацию, содержащую некоторую информацию о классе:
автор, дата создания класса, номер текущей версии, список ревьюеров.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassDescription {
    String author();
    
    String date();
    
    int currentRevision() default 1;
    
    String[] reviewers();
}
