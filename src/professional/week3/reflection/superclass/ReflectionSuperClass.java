package professional.week3.reflection.superclass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Получение родителей
getSuperClass() - вернет Class родителей текущего класса
getInterfaces() - вернет список Class'ов интерфейсов, реализуемых текущим классом.
 */
public class ReflectionSuperClass {
    public static void main(String[] args) {
        //запросить и вывести для класса D все интерфейсы
//        for (Class<?> cls : D.class.getInterfaces()) {
//            System.out.println(cls.getName());
//        }
        
        List<Class<?>> result = getAllInterfaces(D.class);
        for (Class<?> anInterface : result) {
            System.out.println(anInterface.getName());
        }
    }
    
    /*
        Задача 1
        Получить все интерфейсы класса,
        включая интерфейсы от классов-родителей.
        Не включать интерфейсы родительских интерфейсов.
         */
    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
