package week8.oop2.Task3;

import java.util.Arrays;

/*
Примитивная реализация ArrayList
Массив только int, из методов только дбавлять элемент
получать size и увеличивать capacity, когда добавляется новый
 */
public class SimpleArrayList {
    //количество элементов
    private int size;
    private int[] array;
    //объем массива
    private int capacity;
    private static final int DEFAULT_CAPACITY = 5;
    private int currentIndex;

    public SimpleArrayList () {
        array = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currentIndex = 0;
    }
        public SimpleArrayList (int size) {
        array = new int[size];
        capacity = size;
        size = 0;
        currentIndex = 0;
    }
    /*
    Добавляет новый элемент в список массива. При достижении раземера внутреннего
    массива происходит увеличение его в два раза
     */
    public void add(int elem) {
        if(currentIndex >= capacity) {
            capacity = 2 * capacity;
            array = Arrays.copyOf(array, capacity);
            //System.array.copy()
        }
        array[currentIndex] = elem;
        size++;
        currentIndex++;
    }

    public int get(int idx) {
        if(idx < 0 || idx >= size){
            //выбросить исключение (UnSupportedoperationException)
            System.out.println("Невозможно взять элемент по заданному индексу: " + idx);
            return -1;
        } else {
            return array[idx];
        }
    }

    public int size() {
        return size;
    }

    public void remove(int idx) {
        for (int i = idx; i < currentIndex; i++) {
            array[i] = array[i + 1];
        }
        array[currentIndex] = -1;
        currentIndex--;
        size--;
    }
}
