package week8.oop2.Task2;

import java.io.*;
import java.util.Scanner;

public class ReadAndWriteFile {
    //переменная хранит путь до файла
    private static final String FOLDER_DIRECTORY = "D:\\JavaLearn\\SberUniversity\\src\\week8\\oop2\\Task2\\file\\";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    private ReadAndWriteFile(){

    }
    public static void readAndWriteData(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));
        //Scanner scanner = new Scanner(new File(FOLDER_DIRECTORY + "input.txt"));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNextLine()) {
            days[i++] = scanner.nextLine();
        }
        // открыть поток для записи в файл
        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер дня недели " + days[j] + " = " +
                    WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(res);
        }
        //Пример try with resources (Closeable интерфейс) -> не надо закрывать потоки (ресурсы)
        // без закрытых потоков FileWriter подсвечивается
        //try (Writer writer1 = new FileWriter("")){
        //    System.out.println();
        //}

        writer.close();
        scanner.close();
    }
}
