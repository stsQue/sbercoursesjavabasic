package week8.oop2.Task2;

public enum WeekDays {
    MONDAY(1, "понедельник"),
    TUESDAY(2, "вторник"),
    WEDNSDAY(3, "среда"),
    THURSTDAY(4, "четверг"),
    FRIDAY(5,"пятница"),
    SATURDAY(6, "суббота"),
    SUNDAY(7, "воскресенье"),
    NOT_A_DAY(-1, "такого дня недели не существует");

    public final int dayNumber;
    public final String name;
    private static final WeekDays[] ALL = values();
    WeekDays(int dayNumber, String name){
        this.dayNumber = dayNumber;
        this.name = name;
    }

    public static WeekDays ofNumber (int dayNumber) {
        for (WeekDays weekDays: ALL){
            if (weekDays.dayNumber == dayNumber) {
                return weekDays;
            }
        }
        return NOT_A_DAY;
        // throw new IllegalArgumentException("Неизвестный день недели");
    }
    public static WeekDays ofName (String name) {
        for (WeekDays weekDays: ALL){
            if (weekDays.name.equals(name)) {
                return weekDays;
            }
        }
        return NOT_A_DAY;
        // throw new IllegalArgumentException("Неизвестный день недели");
    }
}
