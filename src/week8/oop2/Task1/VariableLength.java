package week8.oop2.Task1;

public class VariableLength {
    static int sum(int... numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i]; //метод переменной длины
        }
        return sum;
    }

    // проверка символа в строке
    // аргумент переменной длины должен быть в конце
    static boolean findChar(Character ch, String... strings) {
        for (String string : strings) {
            if (string.indexOf(ch) != -1) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(sum(1, 2, 3, 4, 5, 6, 7));
        System.out.println(findChar('a', "python", "java"));
        System.out.println(String.format("%d", 123)); // %d - digit, числовая подстановка
        System.out.println(String.format("%s", "asd")); // %s - String подстановка
        // Sring test = """
        // много текста можно заложить в 6 кавычек
        /// """;
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("ad").append("appasdp").reverse().toString();
    }
}
